from django.contrib import admin
from django.contrib.auth.models import User
from .models import Timelog,Project,ProjectMembers,Account,UserProfile
from django.contrib.auth.admin import UserAdmin


 
admin.site.unregister(User)
 
class UserProfileInline(admin.StackedInline):
    model = UserProfile
 
class UserProfileAdmin(UserAdmin):
    inlines = [UserProfileInline]

   
class TimelogAdmin(admin.ModelAdmin):
    fieldsets = [
        (None,               {'fields': ['user','project','time','description','logged']}),
        ('Date information', {'fields': ['task_date']}),
    ]

    
admin.site.register(Timelog,TimelogAdmin)
admin.site.register(Project)

admin.site.register(Account)
admin.site.register(User, UserProfileAdmin)