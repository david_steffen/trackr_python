from rest_api.models import Timelog, Project, UserProfile
from rest_api.serializers import TimelogSerializer, ProjectSerializer, UserSerializer
from rest_framework import generics
from django.contrib.auth.models import User
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.reverse import reverse
from rest_framework.permissions import IsAuthenticated
from rest_framework import viewsets
from rest_framework.decorators import detail_route
from pprint import pprint

class APIRoot(APIView):
    permission_classes = (IsAuthenticated,)

    def get(self, request, format=None):
        response = {
            'users': reverse('user-list', request=request, format=format),
            'timelogs': reverse('timelog-list', request=request, format=format),
            'projects': reverse('project-list', request=request, format=format)
        }
        return Response(response)


# class UserList(generics.ListAPIView):
#     permission_classes = (IsAuthenticated,)
#     queryset = User.objects.all()
#     serializer_class = UserSerializer
# 

# class UserDetail(generics.RetrieveAPIView):
#     permission_classes = (IsAuthenticated,)
#     queryset = User.objects.filter(username=user)
# 
#     def get(self, request, pk, format=None):
#         snippet = self.get_object(pk)
#         serializer = SnippetSerializer(snippet)
#         return Response(serializer.data)
    
class UserViewSet(viewsets.ModelViewSet):
    permission_classes = (IsAuthenticated,)
    serializer_class = UserSerializer
 
    def perform_create(self, serializer):
        serializer.save(user=self.request.user)
 
    def get_queryset(self):
        user = self.request.user
#         print (serializer.is_user_account_manager())
        return User.objects.filter(username=user).select_related('userprofile')

class TimelogViewSet(viewsets.ModelViewSet):
    permission_classes = (IsAuthenticated,)
    serializer_class = TimelogSerializer

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)

    def get_queryset(self):
        user = self.request.user
        return Timelog.objects.filter(user=user)


class ProjectViewSet(viewsets.ModelViewSet):
    permission_classes = (IsAuthenticated,)
    serializer_class = ProjectSerializer

    def get_queryset(self):
        user = self.request.user
        pprint (vars(self.request))
        userObject = User.objects.get(username=user)
        if user.has_perm('can_edit_projects'):
            return Project.objects.filter(account=userObject.userprofile.account)
        else:    
            return Project.objects.filter(members=user)



