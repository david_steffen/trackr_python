from django.db import models
from django.contrib.auth.models import User
from phonenumber_field.modelfields import PhoneNumberField
from django_countries.fields import CountryField



class Account(models.Model):
    name = models.CharField(max_length=50, blank=True)
    phone_numer = PhoneNumberField()
    country = CountryField()
    
    def __str__(self):              # __unicode__ on Python 2
        return self.name
  
class UserProfile(models.Model):
    user = models.OneToOneField(User)
    account = models.ForeignKey(Account)
    

    
      
# class Employee(models.Model):
#     user = models.OneToOneField(User)
#     account = models.CharField(max_length=100)

class Project(models.Model):
    colour = models.CharField(max_length=7)
    abbreviation = models.CharField(max_length=6)
    name = models.CharField(max_length=50)
    company = models.CharField(max_length=50,blank=True)
    account = models.ForeignKey(Account)
    status = models.BooleanField()
    updated = models.DateTimeField(auto_now_add=True)
    created = models.DateTimeField(auto_now_add=True)
    members = models.ManyToManyField(User, through='ProjectMembers')

    class Meta:
        ordering = ('created',)

    def __str__(self):              # __unicode__ on Python 2
        return self.name

class Timelog(models.Model):
    user = models.ForeignKey(User, related_name='timelogs')
    project = models.ForeignKey(Project)
    time = models.PositiveIntegerField()
    description = models.TextField()
    task_date = models.DateTimeField()
    logged = models.BooleanField()
    updated = models.DateTimeField(auto_now_add=True)
    created = models.DateTimeField(auto_now_add=True)

    class Meta:
        ordering = ('created',)

    def __str__(self):              # __unicode__ on Python 2
        return u'%s - %s' % (self.user, self.created)

class ProjectMembers(models.Model):
    user = models.ForeignKey(User)
    project = models.ForeignKey(Project)
    owner = models.BooleanField()
    
    

