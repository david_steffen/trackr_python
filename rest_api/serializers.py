
from rest_framework import serializers
from rest_api.models import Timelog,Project,ProjectMembers
from django.contrib.auth.models import User

# class UserAccountSerializer(serializers.ModelSerializer):
#     serializers.BooleanField(self.context.get('request').user.has_perm('can_edit_projects'))
#     class Meta:
#         model = User
#         fields = ('order', 'title')
        
    

class UserSerializer(serializers.ModelSerializer):
    timelogs = serializers.PrimaryKeyRelatedField(many=True, queryset=Timelog.objects.all())
    
    class Meta:
        model = User
        exclude = ('last_login', 'date_joined')
        
    def is_user_account_manager(self):
        if self.request.user.has_perm('can_edit_projects'):
            return True
        else:
            return False
        
        


class TimelogSerializer(serializers.ModelSerializer):
    user = serializers.ReadOnlyField(source='user.username')

    class Meta:
        model = Timelog
        fields = ('id', 'user', 'project', 'time', 'description', 'task_date', 'logged')

class ProjectSerializer(serializers.ModelSerializer):
    class Meta:
        model = Project
        fields = ('id', 'colour', 'abbreviation', 'name', 'company', 'status','account')
