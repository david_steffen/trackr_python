"""trackr URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.8/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Add an import:  from blog import urls as blog_urls
    2. Add a URL to urlpatterns:  url(r'^blog/', include(blog_urls))
"""
from django.contrib import admin

from django.conf.urls import url, include
from rest_framework.urlpatterns import format_suffix_patterns
from rest_api import views as rest_views
from rest_client import views
from django.contrib.auth import views as auth_views
from rest_api.views import TimelogViewSet, ProjectViewSet, UserViewSet, APIRoot
from rest_framework import renderers

timelog_list = TimelogViewSet.as_view({
    'get': 'list',
    'post': 'create'
})
timelog_detail = TimelogViewSet.as_view({
    'get': 'retrieve',
    'put': 'update',
    'patch': 'partial_update',
    'delete': 'destroy'
})

project_list = ProjectViewSet.as_view({
    'get': 'list',
    'post': 'create'
})
project_detail = ProjectViewSet.as_view({
    'get': 'retrieve',
    'put': 'update',
    'patch': 'partial_update',
    'delete': 'destroy'
})
user_list = UserViewSet.as_view({
    'get': 'list',
    'post': 'create'
})
user_detail = UserViewSet.as_view({
    'get': 'retrieve',
    'put': 'update',
    'patch': 'partial_update',
    'delete': 'destroy'
})
urlpatterns = [
    url(r'^$', views.index, name='index'),
    url(r'^/api/$', rest_views.APIRoot.as_view()),
    url(r'^admin/', include(admin.site.urls)),
    url(r'^api/timelogs/$', timelog_list, name='timelog-list'),
    url(r'^api/timelogs/(?P<pk>[0-9]+)/$', timelog_detail, name='timelog-detail'),
    url(r'^api/projects/$', project_list, name='project-list'),
    url(r'^api/projects/(?P<pk>[0-9]+)/$', project_detail, name='project-detail'),
    url(r'^api/users/$', user_list, name='user-list'),
    url(r'^api/users/(?P<pk>[0-9]+)/$', user_detail, name='user-list'),
    url(r'^api-auth/', include('rest_framework.urls', namespace='rest_framework')),
    url(r'^accounts/login/$', auth_views.login),
]


urlpatterns = format_suffix_patterns(urlpatterns)