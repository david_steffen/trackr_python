from django.shortcuts import render
from django.http import HttpResponse
from django.template import RequestContext, loader
from django.contrib.auth.decorators import login_required
from django.contrib.auth import authenticate, login

@login_required
def index(request):
    template = loader.get_template('times/index.html')

    return render(request, 'times/index.html')


    