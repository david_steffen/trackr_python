

var TaskStore = Fluxxor.createStore({
  initialize: function() {
    this.loading = false;
    this.error = null;
    this.success = false;
    this.tasks= {};

    this.bindActions(
      constants.LOAD_TASK, this.onLoadTask,
      constants.LOAD_TASK_SUCCESS, this.onLoadTaskSuccess,
      constants.LOAD_TASK_FAIL, this.onLoadTaskFail,

      constants.ADD_TASK, this.onAddTask,
      constants.ADD_TASK_SUCCESS, this.onAddTaskSuccess,
      constants.ADD_TASK_FAIL, this.onAddTaskFail,
      
      constants.UPDATE_TASK, this.onUpdateTask,
      constants.UPDATE_TASK_SUCCESS, this.onUpdateTaskSuccess,
      constants.UPDATE_TASK_FAIL, this.onUpdateTaskFail
    );
  },

  onLoadTask: function() {
    this.loading = true;
    this.emit("change");
  },

  onLoadTaskSuccess: function(payload) {
    this.loading = false;
    this.error = null;
    //this.tasks = payload.tasks
    this.tasks = payload.tasks.reduce(function(acc, task) {
      acc[task.id] = {id: task.id, task: task, status: "OK"};
      return acc;
    }, {});
    this.emit("change");
  },

  onLoadTaskFail: function(payload) {
    this.loading = false;
    this.error = payload.err_msg;
    this.emit("change");
  },
  
 onAddTask: function() {
    this.loading = true;
    this.emit("change");
  },

  onAddTaskSuccess: function(payload) {
    this.loading = false;
    this.error = null;
    this.success = true;
    var task = {id: payload.tasks[0].id, task: payload.tasks[0], status: "OK"};
    this.tasks[payload.tasks[0].id] = task;
    this.emit("change");
  },

  onAddTaskFail: function(payload) {
    this.loading = false;
    this.error = payload.err_msg;
    this.emit("change");
  },
  onUpdateTask: function() {
    this.loading = true;
    this.emit("change");
  },

  onUpdateTaskSuccess: function(payload) {
    this.loading = false;
    this.error = null;
    this.success = true;
    var task = {id: payload.tasks[0].id, task: payload.tasks[0], status: "OK"};
    this.tasks[payload.tasks[0].id] = task;
    this.emit("change");
  },

  onUpdateTaskFail: function(payload) {
    this.loading = false;
    this.error = payload.err_msg;
    this.emit("change");
  }
});

var ProjectStore = Fluxxor.createStore({
  initialize: function() {
    this.loading = false;
    this.error = null;
    this.succes = false;
    this.projects= {};

    this.bindActions(
      constants.LOAD_PROJECT, this.onLoadProject,
      constants.LOAD_PROJECT_SUCCESS, this.onLoadProjectSuccess,
      constants.LOAD_PROJECT_FAIL, this.onLoadProjectFail,

      constants.SUBMIT_PROJECT, this.onSubmitProject,
      constants.SUBMIT_PROJECT_SUCCESS, this.onSubmitProjectSuccess,
      constants.SUBMIT_PROJECT_FAIL, this.onSubmitProjectFail
    );
  },

  onLoadProject: function() {
    this.loading = true;
    this.emit("change");
  },

  onLoadProjectSuccess: function(payload) {
    this.loading = false;
    this.error = null;
    //this.projects = payload.projects
    this.projects = payload.projects.reduce(function(acc, project) {
      acc[project.id] = {id: project.id, project: project, status: "OK"};
      return acc;
    }, {});
    this.emit("change");
  },

  onLoadProjectFail: function(payload) {
    this.loading = false;
    this.error = payload.err_msg;
    this.emit("change");
  },

  onSubmitProject: function() {
    this.loading = true;
    this.emit("change");
  },

  onSubmitProjectSuccess: function(payload) {
    this.loading = false;
    this.success = true;
    this.error = null;
    var project = {id: payload.projects[0].id, project: payload.projects[0], status: "OK"};
    this.projects[payload.projects[0]] = project;
    this.emit("change");
  },

  onSubmitProjectFail: function(payload) {
    this.loading = false;
    this.error = payload.err_msg;
    this.emit("change");
  }
});

var stores = {
  TaskStore: new TaskStore(),
  ProjectStore: new ProjectStore()
};

var flux = new window.Fluxxor.Flux(stores, actions);



flux.on("dispatch", function(type, payload) {
  if (console && console.log) {
    console.log("[Dispatch]", type, payload);
  }
});

