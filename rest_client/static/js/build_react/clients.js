var TaskClient = {
    load: function(success, failure) {
        $.ajax({type: "GET",url:'/tracker/view/'})
        .done(function(_data){
            if(_data.payload.success){ 
                success(_data.payload.tasks);
            }
        }).fail(function(_data){
            failure(_data.responseJSON.payload);
        });
    },
    add: function(tasks, success, failure) {
        $.ajax({type: "POST",url:'/tracker/add',data: tasks})
        .done(function(_data){
            if(_data.payload.success){
                success(_data.payload.tasks);
            } else {
                failure(_data.payload.err_msg);
            }
        }).fail(function(_data){
                failure(_data.responseJSON.payload);
        });
    },
    update: function(tasks, success, failure) {
        $.ajax({type: "POST",url:'/tracker/update',data: tasks})
        .done(function(_data){
            if(_data.payload.success){
                success(_data.payload.tasks);
            } else {
                failure(_data.payload.err_msg);
            }
        }).fail(function(_data){
                failure(_data.responseJSON.payload);
        });
    },
    delete: function(tasks, success, failure) {
        $.ajax({type: "POST",url:'/tracker/delete',data: tasks})
        .done(function(_data){
            if(_data.payload.success){
                success(_data.payload.tasks);
            } else {
                failure(_data.payload.err_msg);
            }
        }).fail(function(_data){
                failure(_data.responseJSON.payload);
        });
    },
};

var ProjectClient = {
    load: function(success, failure) {
        $.ajax({type: "GET",url:'/projects/details/'})
        .done(function(_data){
            if(_data.payload.success){ 
                success(_data.payload.projects);
            }
        }).fail(function(_data){
            failure(_data.responseJSON.payload);
        });
    },

    submit: function(action,projects, success, failure) {
        $.ajax({type: "POST",url:'/projects/'+action,data: projects})
        .done(function(_data){
            if(_data.payload.success){
                success(_data.payload.projects);
            } else {
                failure(_data.payload.err_msg);
            }
        }).fail(function(_data){
                failure(_data.responseJSON.payload);
        });
    }
};
