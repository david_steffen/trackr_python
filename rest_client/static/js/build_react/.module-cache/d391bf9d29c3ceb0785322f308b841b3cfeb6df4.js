var FormTextInput = React.createClass({displayName: "FormTextInput",
    componentWillReceiveProps: function(nextProps) {
        var value = nextProps.value
        if(typeof value !== 'undefined') {
             React.findDOMNode(this).value = value
        }
        
    },
    render: function() {
        return  React.createElement("input", {placeholder: this.props.placeholder, className: this.props.classes, type: "text", ref: this.props.name, name: this.props.name, onChange: this.handleChange})
    },
    handleChange: function(event) {
        this.props.onChange(event.target.name, event.target.value);
    }
})
var FormButton = React.createClass({displayName: "FormButton",
    
    render: function() {
        return  React.createElement("input", {onClick: this.handleClick, className: this.props.classes, value: this.props.value, type: this.props.type, ref: this.props.name, name: this.props.name})
    },
    handleClick: function() {
        if(typeof this.props.onClick === 'function') {
            this.props.onClick();
        }
    },
})

var TaskForm = React.createClass({displayName: "TaskForm",
    getInitialState: function() {
        return {
            project_id: null,
            task_date: null,
            id: null,
            //reset: false,
        };
    },
    componentDidMount: function() {
        
    },
    componentWillReceiveProps: function(nextProps) {
        console.log(nextProps)
        var task = nextProps.task
        if(typeof task !== 'undefined' && nextProps.update) {
            React.findDOMNode(this.refs.time).value = task.time;
            React.findDOMNode(this.refs.description).value = task.description;
            this.setState({project_id: task.project_id});
            this.setState({task_date: task.task_date});
            this.setState({id: task.id});
        }
        if(nextProps.reset) {
            this.cancelForm('moo');
        }
    },
    render: function() {
         var classes = {
            'add-item': true,
            'open': this.props.open
        };
        var openClasses = classSet(classes);
        return (

            React.createElement("form", {onSubmit: this.handleSubmitForm}, 
            React.createElement("ul", {className: openClasses}, 
                            React.createElement("li", null, 
                                React.createElement("label", null, "Project:"), 
                                React.createElement(ProjectDropdown, {reset: this.state.reset, project_id: this.state.project_id, onClick: this.handleProject})
                            ), React.createElement("li", null, 
                                React.createElement("label", null, "Time:"), 
                                React.createElement("input", {className: "form-input short", placeholder: "0.0", name: "time", ref: "time"})
                            ), React.createElement("li", null, 
                                React.createElement("label", null, "Description:"), 
                                React.createElement("input", {className: "form-input long", placeholder: "Add description here", name: "description", ref: "description"})
                            ), React.createElement("li", null, 
                                React.createElement("label", null, "Date:"), 
                                React.createElement(DayPicker, {reset: this.state.reset, task_date: this.state.task_date, onClick: this.handleDate})
                            ), React.createElement("li", null, 
                                React.createElement("div", {className: "form-buttons-wrap"}, 
                                    React.createElement("div", {className: "button"}, 
                                    this.props.children

        /*<FormButton classes='right' value='Cancel' type='button' name='cancel' onClick={this.cancelForm} />
                                        <FormButton classes='right' value='Submit' type='submit' name='log' />*/
                                    )
                                )
                            )
                      )
            
          )
        );
    },
    updateTaskState: function(key, value) {
//        var input= (this.state.task)? this.state.task: {task:{}};
//        input[ key ] = value;
//        this.setState(input);
    },
    handleProject: function (id) {
        this.setState({project_id: id});
    },
    handleChange: function(event) {
        //this.updateTaskState(event.target.name, event.target.value);
        //this.props.onChange(event)
    },
    handleSubmitForm: function(e) {
        e.preventDefault();
        var dateHelper = new DateHelper();
        var date = (dateHelper.isJSTimestamp(this.state.task_date)) ? dateHelper.createUnixTimestamp(this.state.task_date): this.state.task_date;
        var data = {
            id: this.state.id,
            time: React.findDOMNode(this.refs.time).value,
            description: React.findDOMNode(this.refs.description).value,
            project_id: this.state.project_id,
            task_date: date
        }
        this.props.onSubmit(data);
    },
    handleDate: function (date) {
        this.setState({task_date: date.timestamp});
        //this.updateTaskState('task_date', date.timestamp);
    },
    cancelForm: function() {
        var that =this;
        React.findDOMNode(this.refs.time).value = '';
        React.findDOMNode(this.refs.description).value = '';
        this.setState({project_id: null});
        this.setState({task_date: null});
        this.setState({id: null});
    },
    formatDate: function(timestamp) {
        var dateHelper = new DateHelper();
        return dateHelper.createUnixTimestamp(timestamp);
    },
});