var TaskRow  = React.createClass({displayName: "TaskRow",
    getInitialState: function() {
      return {
          edit: false,
      }  
    },
    render: function() {
        var dateHelper = new DateHelper();
        var t_stamp = dateHelper.createJSTimestamp(this.props.task.task_date);
        var date = new Date(t_stamp);
        dateHelper.setDateObject(date);
        var formattedDate =  dateHelper.humanReadible();
        var classes = {
            'list-row': true,
            'edit': this.state.edit
        };
        var editableClasses = classSet(classes);
        var divStyle = {
            backgroundColor: this.props.task.colour,
        };
        
        return (
            React.createElement("ul", {"data-id": this.props.task.id, className: editableClasses}, 
                React.createElement("li", {className: "list-item list-date", ref: "date"}, formattedDate), 
                React.createElement("li", {className: "list-item list-time", ref: "time"}, React.createElement("span", {className: "fa fa-clock-o"}), this.props.task.time), 
                React.createElement("li", {className: "list-item list-abr", ref: "project"}, React.createElement("div", {className: "project-colour", style: divStyle}), this.props.task.abr), 
                React.createElement("li", {className: "list-item list-description", ref: "description"}, this.props.task.description), 
                React.createElement("li", {className: "list-item list-edit"}, React.createElement("span", {onClick: this.handleClick, className: "fa fa-pencil-square"}))
            )
        );
    },
    handleClick: function(e) {
        //this.setState({edit:true})
        this.props.onClick(this.props.task.id)
//        React.findDOMNode(this.refs.time).contentEditable = true;
//        React.findDOMNode(this.refs.description).contentEditable = true;
    },
});

var TaskList = React.createClass({displayName: "TaskList",
    getInitialState: function() {
      return {
          edit: false,
      }  
    },
    render: function() {
        var classes = {
            'edit-wrap': true,
            'edit': this.state.edit
        };
        var editableClasses = classSet(classes);
        return (
            React.createElement(ReactCSSTransitionGroup, {transitionName: "list-table"}, 
            this.props.tasks.map(function (task) {
                   return React.createElement(TaskRow, {onClick: this.handleClick, key: task.task.id, task: task.task})
                }.bind(this))
            )
        );
    },
    handleClick: function(id) {
        this.setState({edit: true})
        this.props.onClick(id)
    },
});