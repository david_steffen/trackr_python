var Login = React.createClass({displayName: "Login",

  getInitialState: function() {
    return {
      email: "",
      password: ""
    };
  },


  render: function() {
    return (
        React.createElement("form", {onSubmit: this.handleSubmitForm}, 
            React.createElement("div", null, 
                React.createElement("div", null, 
                    React.createElement("label", {for: "email"}, "Email", React.createElement("span", {class: "required"}, "*")), 
                    React.createElement("input", {type: "text", name: "email", ref: "email"})
                ), 
                React.createElement("div", null, 
                    React.createElement("label", {for: "password"}, "Password", React.createElement("span", {class: "required"}, "*")), 
                    React.createElement("input", {type: "password", name: "password", ref: "password"})
                ), 
                React.createElement("div", null, 
                    React.createElement("div", {class: "button"}, " ", React.createElement("input", {value: "Login", name: "login", type: "submit"}))
                )
            )
        )
    );
  },

  handleSubmitForm: function(e) {
    e.preventDefault();
    var email = React.findDOMNode(this.refs.email).value.trim();
    var password = React.findDOMNode(this.refs.password).value.trim();
    console.log(email);
    $.ajax({type: "POST",url:'/users/login',data: {email: email, password:password}})
                .done(function(_data){
            console.log(_data);
        });

  }
});
if(document.getElementById('login')) {
React.render(
    React.createElement(Login, null),
    document.getElementById('login')
  );
}