
var ProjectRow  = React.createClass({displayName: "ProjectRow",
    getInitialState: function() {
      return {
          edit: false,
      }  
    },
    render: function() {
//        var dateHelper = new DateHelper();
//        var t_stamp = dateHelper.createJSTimestamp(this.props.task.task_date);
//        var date = new Date(t_stamp);
//        dateHelper.setDateObject(date);
//        var formattedDate =  dateHelper.humanReadible();
        var classes = {
            'list-row': true,
            'edit': this.state.edit
        };
        var editableClasses = classSet(classes);
        var total_time = this.calcTimes(this.props.project.total_times)
        var divStyle = {
            backgroundColor: this.props.project.colour,
          };
        return (

                React.createElement("ul", {"data-id": this.props.project.id, "data-total": total_time, className: editableClasses}, 
                /*<li className='project-item-date' ref='date'>{formattedDate}</li>*/
                    React.createElement("li", {className: "list-item list-colour", ref: "colour"}, React.createElement("div", {className: "project-colour", style: divStyle})), 
                    React.createElement("li", {className: "list-item list-abr", ref: "abbreviation"}, this.props.project.abbreviation), 
                    React.createElement("li", {className: "list-item list-name", ref: "name"}, this.props.project.name), 
                    React.createElement("li", {className: "list-item list-total", ref: "total-time"}, total_time), 
                    React.createElement("li", {className: "list-item list-edit"}, React.createElement("span", {onClick: this.handleClick, className: "fa fa-pencil-square"}))
                )

        );
    },
    calcTimes: function(times) {
        if(!times) { return 0; }
        var total_time = 0;
        for(x in times) {
            total_time += parseFloat(times[x].time);
        }
        return total_time;
    },
    handleClick: function(e) {
        this.props.onClick(this.props.project.id)
    },
});

var ProjectList = React.createClass({displayName: "ProjectList",
    getInitialState: function() {
      return {
          edit: false,
      }  
    },
    render: function() {
        var classes = {
            'edit-wrap': true,
            'edit': this.state.edit
        };
        var editableClasses = classSet(classes);
        return (
            React.createElement(ReactCSSTransitionGroup, {transitionName: "project-table"}, 
            this.props.projects.map(function (project) {
                   return React.createElement(ProjectRow, {onClick: this.handleClick, key: project.project.id, project: project.project})
                }.bind(this))
            )
        );
    },
    handleClick: function(id) {
        this.setState({edit: true})
        this.props.onClick(id)
    },
});