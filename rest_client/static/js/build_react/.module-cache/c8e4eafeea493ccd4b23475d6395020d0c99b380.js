var DayItem = React.createClass({displayName: "DayItem",
    getInitialState: function() {
        return {
            selected: false,
        };
    },
    render: function() {
        var dateObj = new Date();
        var dateHelper = new DateHelper();
        dateHelper.setDateObject(dateObj);
        var currentDate = dateHelper.humanReadible();
        if(currentDate === this.props.date.formattedDate){
            var dateClasses = 'day-picker selected'
        } else {
            var dateClasses =  'day-picker'
        }
        return (
            React.createElement("div", {onClick: this.handleDate, className: dateClasses}, this.props.date.formattedDate)
        );
    },
    handleDate: function(e) {
        $(this.getDOMNode()).addClass('selected').siblings().removeClass('selected');
        this.props.onClick(this.props.date);
    },
});
var DayPicker = React.createClass({displayName: "DayPicker",
    getInitialState: function() {
        return {
            error: false,
            err_msg: "",
            dates: [],
            lastDate: '',
            currentDate: '',
            numberOfDays: 5,
        };
    },
    render: function() {
        var dateNodes = this.state.dates.map(function (date,x) {
            return (
                React.createElement(DayItem, {key: x, onClick: this.handleDate, date: date})
            );
        }.bind(this));
        return (
            React.createElement("div", {className: "day-picker-wrap"}, 
                React.createElement("div", {onClick: this.handlePreviousDates, className: "day-picker date-prev fa fa-chevron-left"}), 
                React.createElement("div", {className: "day-picker-inner-wrap"}, 
                    dateNodes
                ), 
                React.createElement("div", {onClick: this.handleNextDates, className: "day-picker date-next fa fa-chevron-right"})
            )
        );
    },
    componentWillReceiveProps: function(nextProps) {
        console.log(nextProps)
        var dateHelper = new DateHelper();
        if(typeof nextProps.task_date !== 'undefined' && !dateHelper.isJSTimestamp(nextProps.task_date)) {
            var date = dateHelper.createJSTimestamp(nextProps.task_date);
            var dateObj = new Date(date);
            this.setDates(dateObj, this.state.numberOfDays);
            var that = this;
            setTimeout(function(){
                $(that.getDOMNode()).find('.day-picker-inner-wrap .day-picker').removeClass('selected').filter(':last-child').addClass('selected');
            },20)  
        }
//        if(nextProps.reset){
//            var dateObj = new Date();
//            this.setDates(dateObj, this.state.numberOfDays);
//        }
    },
//    componentWillUpdate: function(nextProps, nextState) {
//        console.log(nextProps)
//        if(nextProps.reset){
//            var dateObj = new Date();
//            this.setDates(dateObj, this.state.numberOfDays);
//        }
//    },
    componentDidMount: function() {
        var screenWidth = $( window ).width();
        var numberOfDays;
        if(screenWidth <= 768) {
            this.setState({numberOfDays: 3});
            numberOfDays = 3;
        } else {
            this.setState({numberOfDays: 5});
            numberOfDays = 5;
        }
        var dateObj = new Date();
        this.setDates(dateObj,numberOfDays);

    },
    handlePreviousDates: function(e) {
        this.removeSelectedClass(e.target);
        var dateObj = new Date(this.state.lastDate);
        this.setDates(dateObj, this.state.numberOfDays);
    },
    setDates: function(dateObj, numberOfDays) {
        var dateHelper = new DateHelper();
        dateHelper.setDateObject(dateObj);
        this.setState({currentDate: dateHelper.getTimestampString()});
        var datesArray =  dateHelper.getPreviousDates(dateObj, numberOfDays);
        this.setState({dates: datesArray});
        var lastDate = dateHelper.getTimestampString();
        this.setState({lastDate: lastDate});
    },
    removeSelectedClass:function(el) {
        $(el).siblings('.day-picker-inner-wrap').find('.selected').removeClass('selected');
    },
    handleNextDates: function(e) {
        this.removeSelectedClass(e.target);
        var dateObj = new Date(this.state.currentDate);
        var dateHelper = new DateHelper();
        dateHelper.setDateObject(dateObj);
        this.setState({lastDate: dateHelper.getTimestampString()});
        var datesArray =  dateHelper.getNextDates(dateObj, this.state.numberOfDays);
        this.setState({dates: datesArray});
        var currentDate = dateHelper.getTimestampString();
        this.setState({currentDate: currentDate});
    },
    handleDate: function (date) {
        this.props.onClick(date);
    },
});