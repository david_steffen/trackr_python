var constants = {
    LOAD_TASK: "LOAD_TASK",
    LOAD_TASK_SUCCESS: "LOAD_TASK_SUCCESS",
    LOAD_TASK_FAIL: "LOAD_TASK_FAIL",

    SUBMIT_TASK: "SUBMIT_TASK",
    SUBMIT_TASK_SUCCESS: "SUBMIT_TASK_SUCCESS",
    SUBMIT_TASK_FAIL: "SUBMIT_TASK_FAIL",
    
    LOAD_PROJECT: "LOAD_PROJECT",
    LOAD_PROJECT_SUCCESS: "LOAD_PROJECT_SUCCESS",
    LOAD_PROJECT_FAIL: "LOAD_PROJECT_FAIL",

    SUBMIT_PROJECT: "SUBMIT_PROJECT",
    SUBMIT_PROJECT_SUCCESS: "SUBMIT_PROJECT_SUCCESS",
    SUBMIT_PROJECT_FAIL: "SUBMIT_PROJECT_FAIL"
};

var actions = {
    loadTask: function() {
        this.dispatch(constants.LOAD_TASK);
        
        TaskClient.load(function(tasks) {
            this.dispatch(constants.LOAD_TASK_SUCCESS, {tasks: tasks});
        }.bind(this), function(error) {
            this.dispatch(constants.LOAD_TASK_FAIL, {error: error});
        }.bind(this));
    },
    addTask: function(task) {
        this.dispatch(constants.SUBMIT_TASK, {task: task});

        TaskClient.add(task, function(tasks) {
            this.dispatch(constants.SUBMIT_TASK_SUCCESS, {tasks: tasks});
        }.bind(this), function(error) {
            this.dispatch(constants.SUBMIT_TASK_FAIL, {error: error});
        }.bind(this));
    },
    updateTask: function(task) {
        this.dispatch(constants.SUBMIT_TASK, {task: task});

        TaskClient.update(task, function(tasks) {
            this.dispatch(constants.SUBMIT_TASK_SUCCESS, {tasks: tasks});
        }.bind(this), function(error) {
            this.dispatch(constants.SUBMIT_TASK_FAIL, {error: error});
        }.bind(this));
    },
    
    
    loadProject: function() {
        this.dispatch(constants.LOAD_PROJECT);
        
        ProjectClient.load(function(projects) {
            this.dispatch(constants.LOAD_PROJECT_SUCCESS, {projects: projects});
        }.bind(this), function(error) {
            this.dispatch(constants.LOAD_PROJECT_FAIL, {error: error});
        }.bind(this));
    },
    submitProject: function(project) {
        this.dispatch(constants.SUBMIT_PROJECT, {project: project});

        ProjectClient.submit('add',project, function(projects) {
            this.dispatch(constants.SUBMIT_PROJECT_SUCCESS, {projects: projects});
        }.bind(this), function(error) {
            this.dispatch(constants.SUBMIT_PROJECT_FAIL, {error: error});
        }.bind(this));
    },
    updateProject: function(project) {
        this.dispatch(constants.SUBMIT_PROJECT, {project: project});

        ProjectClient.submit('update',project, function(projects) {
            this.dispatch(constants.SUBMIT_PROJECT_SUCCESS, {projects: projects});
        }.bind(this), function(error) {
            this.dispatch(constants.SUBMIT_PROJECT_FAIL, {error: error});
        }.bind(this));
    }
};