//var cx = React.addons.classSet;

var DayItem = React.createClass({displayName: "DayItem",
    getInitialState: function() {
        return {
            selected: false,
        };
    },
    render: function() {
        var dateNodes = this.props.data.map(function (date,x) {
            var dateObj = new Date();
            var dateHelper = new DateHelper();
            dateHelper.setDateObject(dateObj);
            var currentDate = dateHelper.humanReadible();
            if(currentDate === date.formattedDate){
            var dateClasses = 'day-picker selected'
            } else {
               var dateClasses =  'day-picker'
            }
            return (
                React.createElement("div", {key: x, "data-date": date.timestamp, onClick: this.handleDate, className: dateClasses}, date.formattedDate)
            );
        }.bind(this));
        return (
            React.createElement("div", {className: "day-picker-inner-wrap"}, 
                dateNodes
            )
        );
    },
    handleDate: function(e) {
        $(e.target).addClass('selected').siblings().removeClass('selected');
        this.props.onClick(e.target.dataset.date);
    },
});
var DayPicker = React.createClass({displayName: "DayPicker",
    getInitialState: function() {
        return {
            error: false,
            err_msg: "",
            data: [],
            lastDate: '',
            currentDate: ''
        };
    },
    render: function() {
        return (
            React.createElement("div", {className: "day-picker-wrap"}, 
                React.createElement("div", {onClick: this.handlePreviousDates, className: "day-picker date-prev fa fa-chevron-left"}), 
                React.createElement(DayItem, {onClick: this.handleDate, data: this.state.data}), 
                React.createElement("div", {onClick: this.handleNextDates, className: "day-picker date-next fa fa-chevron-right"})
            )
        );
    },
    componentDidMount: function() {
        var dateObj = new Date();
        var dateHelper = new DateHelper();
        dateHelper.setDateObject(dateObj);
        this.setState({currentDate: dateHelper.getTimestampString()});
        var datesArray =  dateHelper.getPreviousDates(dateObj, 3);
        this.setState({data: datesArray});
        var lastDate = dateHelper.getTimestampString();
        this.setState({lastDate: lastDate});
    },
    handlePreviousDates: function(e) {
        this.removeSelectedClass(e.target)
        var dateObj = new Date(this.state.lastDate);
        var dateHelper = new DateHelper();
        dateHelper.setDateObject(dateObj);
        this.setState({currentDate: dateHelper.getTimestampString()});
        var datesArray =  dateHelper.getPreviousDates(dateObj, 3);
        this.setState({data: datesArray});
        var lastDate = dateHelper.getTimestampString();
        this.setState({lastDate: lastDate});
    },
    removeSelectedClass: function(el) {
        $(el).siblings('.day-picker-inner-wrap').find('.selected').removeClass('selected');
    },
    handleNextDates: function(e) {
        this.removeSelectedClass(e.target)
        var dateObj = new Date(this.state.currentDate);
        var dateHelper = new DateHelper();
        dateHelper.setDateObject(dateObj);
        this.setState({lastDate: dateHelper.getTimestampString()});
        var datesArray =  dateHelper.getNextDates(dateObj, 3);
        this.setState({data: datesArray});
        var currentDate = dateHelper.getTimestampString();
        this.setState({currentDate: currentDate});
    },
    handleDate: function (date) {
        this.setState({date: date});
        this.props.onClick(date);
    },
});
var ProjectRow = React.createClass({displayName: "ProjectRow",
    render: function() {
        return (
            React.createElement("div", {onClick: this.handleClick, "data-id": this.props.id, "data-abr": this.props.abbreviation}, 
                React.createElement("span", {className: "project-item-abr"}, this.props.abbreviation), 
                React.createElement("span", {className: "project-item-name"}, this.props.name)
            )
        );
    },
    handleClick: function(e) {
        var el = this.getDOMNode();
        this.props.onClick(el.dataset.id, el.dataset.abr);
    }
});
var ProjectList = React.createClass({displayName: "ProjectList",
    getInitialState: function() {
        return {
            error: false,
            err_msg: "",
            data: [],
            id: '',
            active: false,
            activeClass: 'active',
            project: ''
        };
    },
    render: function() {
        var classes = {
            'project-list': true,
            'active': this.state.active
        };
        var dropdownClasses = cx(classes);
        var selectedOption = (this.state.id) ? this.state.project : 'Please select...';
        var projectNodes = this.state.data.map(function (project) {
            return (
                React.createElement("div", {key: project.id}, 
                    React.createElement(ProjectRow, React.__spread({onClick: this.handleClick},  project))
                )
            );
        }.bind(this));
        return (
            React.createElement("ul", {"data-selected-id": this.state.id, className: dropdownClasses}, 
                React.createElement("li", {onClick: this.handleDropDown, className: "project-list-selection"}, selectedOption, React.createElement("span", {className: "fa fa-chevron-down"})), 
                React.createElement("li", {className: "project-list-wrap"}, 
                projectNodes
                )
            ) 
        );
    },
    loadProjectsFromServer: function() {
        var that = this;
        $.ajax({type: "GET",url:'/projects/view'})
        .done(function(_data){
            if(_data.payload.success){
                that.setState({data: _data.payload.projects});
            } else {
                that.setState({error: true});
            }
        }).fail(function(_data){
            that.setState({error: true});
            that.setState({err_msg: _data.responseJSON.payload.err_msg});
        });
    },
    componentDidMount: function() {
        this.loadProjectsFromServer();
    },
    handleClick: function (id,project) {
        this.props.onClick(id);
        this.setState({id: id});
        this.setState({project: project});
        this.handleDropDown();
    },
    handleDropDown: function(e) {
        if(this.state.active) {
            this.setState({active: false});
        } else {
            this.setState({active: true});
        }
    }
});
var TaskForm = React.createClass({displayName: "TaskForm",
    getInitialState: function() {
        return {
            time: '',
            description: '',
            project_id: '',
            date: '',
        };
    },
    render: function() {
        return (
            React.createElement("form", {onSubmit: this.handleSubmitForm}, 
                React.createElement("ul", {className: "add-task"}, 
                    React.createElement("li", null, 
                        React.createElement("label", null, "Project:"), 
                            React.createElement(ProjectList, {onClick: this.handleClick})
                    ), 
                    React.createElement("li", null, 
                        React.createElement("label", {for: "time"}, "Time:"), 
                        React.createElement("input", {placeholder: "0.0", className: "form-input", type: "text", ref: "time", name: "time", onChange: this.handleChange})
                    ), 
                    React.createElement("li", null, 
                        React.createElement("label", {for: "description"}, "Description:"), 
                        React.createElement("input", {placeholder: "Add description here", className: "form-input", ref: "description", type: "text", name: "description", onChange: this.handleChange})
                    ), 
                    React.createElement("li", null, 
                        React.createElement("label", null, "Date:"), 
                        React.createElement(DayPicker, {onClick: this.handleDate})
                    ), 
                    React.createElement("li", null, 
                        React.createElement("div", {className: "button"}, React.createElement("input", {value: "Add task", name: "log", type: "submit"}))
                    )
              )
          )
        );
    },
    handleClick: function (id) {
        this.setState({project_id: id});
    },
    handleChange: function(event) {
        var input = {}
        input[ event.target.name ] = event.target.value
        this.setState(input);
    },
    handleSubmitForm: function(e) {
        e.preventDefault();
        this.props.onSubmit(this);
    },
    handleDate: function (date) {
        this.setState({date: date});
    },
    clearFormInputs: function() {
        React.findDOMNode(this.refs.time).value = '';
        React.findDOMNode(this.refs.description).value = '';
    },
});



