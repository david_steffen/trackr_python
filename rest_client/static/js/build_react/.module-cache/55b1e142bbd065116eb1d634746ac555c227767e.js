var Projects = React.createClass({displayName: "Projects",
    mixins: [FluxMixin, StoreWatchMixin("ProjectStore")],
    getInitialState: function() {
        return {
            projectToEdit: [],
            update: false
        };
    },
    getStateFromFlux: function() {
        var store = this.getFlux().store("ProjectStore");
        return {
          loading: store.loading,
          error: store.error,
          projects: _.values(store.projects),
          success: store.success,
        };
    },
    render: function() {
        var formHeading = (this.state.update) ? 'Edit project' : 'Add a project';
        return (
            React.createElement("div", null, 
                React.createElement("div", {className: "project-form-wrap"}, 
                    React.createElement("h1", null, React.createElement("span", {className: "fa fa-chevron-right"}), formHeading), 
                    React.createElement(ProjectForm, {onClick: this.cancelForm, onSubmit: this.handleSubmit, update: this.state.update, project: this.state.projectToEdit})
                ), 
                React.createElement("div", {className: "project-list-wrap"}, 
                    React.createElement("h1", null, React.createElement("span", {className: "fa fa-chevron-right"}), "Projects"), 
                    this.state.error ? "<span>Error loading data</span>" : null, 
                    this.state.loading ? React.createElement("span", null, "Loading...") : null, 
                    React.createElement(ProjectList, {projects: this.state.projects, onClick: this.handleEditProject})
                ), 
                React.createElement("div", {className: "project-stats-wrap"}
                
                )
            )
        );
    },
    handleSubmit: function(props) {
        (this.state.update) ? this.getFlux().actions.updateProject(props) : this.getFlux().actions.submitProject(props);
        if(this.state.success) {
            this.setState({update: false});
        }
    },
    handleEditProject: function(id) {
        this.setState({update: true});
        for(var x = 0; x < this.state.projects.length; x++) {
            if(this.state.projects[x].id === id) {
                var project = this.state.projects[x].project;
                this.setState({projectToEdit: project});
                break;
            }
            
        }
    },
    cancelForm: function() {
        this.setState({update: false});
    },
    componentDidMount: function() {
        this.getFlux().actions.loadProject();
        //this.loadTasksFromServer(0);
    },
});

var Tracker = React.createClass({displayName: "Tracker",
    mixins: [FluxMixin, StoreWatchMixin("TaskStore")],
    getInitialState: function() {
        return {
            taskToEdit: [],
            update: false,
            fixed: false,
            openForm: false,
        };
    },
    getStateFromFlux: function() {
        var store = this.getFlux().store("TaskStore");
        return {
          loading: store.loading,
          error: store.error,
          tasks: _.values(store.tasks).reverse(),
          success: store.success,
        };
    },
    render: function() {
        var formHeading = (this.state.update) ? 'Edit task' : 'Add a task';
        var classes = {
            'form-wrap': true,
            'fixed': true
        };
        var formClasses = classSet(classes);
        var classes = {
            'list-wrap': true,
            'fixed': true
        };
        var listClasses = classSet(classes);
        return (
            React.createElement("div", null, 
                React.createElement("div", {className: formClasses}, 
                                React.createElement("div", {className: "form-inner-wrap"}, 
                    React.createElement("h1", {onClick: this.toggleForm, className: "form-title"}, React.createElement("span", {className: "fa fa-chevron-right"}), formHeading), 
                    React.createElement(TaskForm, {onClick: this.cancelForm, open: this.state.openForm, success: this.state.success, onSubmit: this.handleSubmit, update: this.state.update, task: this.state.taskToEdit})
                    )
                ), 
                React.createElement("div", {className: listClasses}, 
                    React.createElement("h1", null, React.createElement("span", {className: "fa fa-chevron-right"}), "Tasks"), 
                    this.state.error ? "<span>Error loading data</span>" : null, 
                    this.state.loading ? React.createElement("span", null, "Loading...") : null, 
                    React.createElement(TaskList, {tasks: this.state.tasks, onClick: this.handleEditTask})
                )
            )
        );
    },
    toggleForm: function() {
        if(this.state.openForm) {
            this.setState({openForm: false});
        } else {
            this.setState({openForm: true});
        }
    },
    handleSubmit: function(props) {
        (this.state.update) ? this.getFlux().actions.updateTask(props) : this.getFlux().actions.submitTask(props);
        if(this.state.success) {
            this.setState({update: false});
        }
    },
    shouldComponentUpdate: function(nextProps, nextState) {
        return !this.state.update
    },
    handleEditTask: function(id) {
        if(this.state.update) {
            this.setState({taskToEdit: null});
        }
        this.setState({update: true});
        for(var x = 0; x < this.state.tasks.length; x++) {
            if(this.state.tasks[x].id === id) {
                var task = this.state.tasks[x].task;
                this.setState({taskToEdit: task});
                break;
            }
            
        }
    },
    cancelForm: function() {
        this.setState({update: false});
    },
    componentDidMount: function() {
        this.getFlux().actions.loadTask();
        var top = $('.form-wrap').offset().top - parseFloat($('.form-wrap').css('margin-top').replace(/auto/, 0));
        var that = this;
        var formHeight = $('.form-wrap').outerHeight();
        $(window).scroll(function (event) {
            // what the y position of the scroll is
            var y = $(this).scrollTop()-formHeight;
            console.log(y);
            console.log(top)
            console.log(y-formHeight);
            // whether that's below the form
            if (y >= top) {
                // if so, ad the fixed class
//                that.setState({fixed: true});
//                $(that.getDOMNode()).find('.form-wrap').css('margin-top',formHeight+'px')
            } else {
                // otherwise remove it
//                that.setState({fixed: false});
                $(that.getDOMNode()).find('.form-wrap').css('margin-top',y+'px')
            }
        });
    },
});





if(document.getElementById('tracker')) {   
    React.render(
        React.createElement(Tracker, {flux: flux}),
        document.getElementById('tracker')
    );
}

if(document.getElementById('projects')) {   
    React.render(
        React.createElement(Projects, {flux: flux}),
        document.getElementById('projects')
    );
}

