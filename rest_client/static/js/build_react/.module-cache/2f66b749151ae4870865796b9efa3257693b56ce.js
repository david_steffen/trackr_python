var TaskFormTextInput = React.createClass({displayName: "TaskFormTextInput",
    getInitialState: function() {
        return {
            value: null,
        };
    },
    componentWillReceiveProps: function(nextProps) {
        var value = nextProps.value
        if(typeof value !== 'undefined') {
            this.getDOMNode().value = value
        }
        
    },
    render: function() {
        return  React.createElement("input", {placeholder: placeholdr, className: classes, type: "text", ref: name, name: name})
    },
})

var TaskForm = React.createClass({displayName: "TaskForm",
    getInitialState: function() {
        return {
            project_id: null,
            task_date: null,
            id: null,
            reset: false,
        };
    },
    componentDidMount: function() {
        
    },
    componentWillReceiveProps: function(nextProps) {
        var task = nextProps.task
        if(typeof task !== 'undefined' && nextProps.update) {
            React.findDOMNode(this.refs.time).value = task.time,
            React.findDOMNode(this.refs.description).value = task.description,
            this.setState({project_id: task.project_id});
            this.setState({task_date: task.task_date});
            this.setState({id: task.id});
        }
        
    },
    render: function() {
        
        //var deleteButton = (this.props.update) ? '<input type="button"
        return (

            React.createElement("form", {onSubmit: this.handleSubmitForm}, 
            this.props.children
          )
        );
    },
    updateTaskState: function(key, value) {
//        var input= (this.state.task)? this.state.task: {task:{}};
//        input[ key ] = value;
//        this.setState(input);
    },
    handleClick: function (id) {
        this.setState({project_id: id});
    },
    handleChange: function(event) {
        //this.updateTaskState(event.target.name, event.target.value);
        //this.props.onChange(event)
    },
    handleSubmitForm: function(e) {
        e.preventDefault();
        var dateHelper = new DateHelper();
        var date = (dateHelper.isJSTimestamp(this.state.task_date)) ? dateHelper.createUnixTimestamp(this.state.task_date): this.state.task_date;
        var data = {
            id: this.state.id,
            time: React.findDOMNode(this.refs.time).value,
            description: React.findDOMNode(this.refs.description).value,
            project_id: this.state.project_id,
            task_date: date
        }
        this.props.onSubmit(data);
    },
    handleDate: function (date) {
        this.updateTaskState('task_date', date.timestamp);
    },
    cancelForm: function() {
        var that =this;
        this.setState({reset: true},function() {
            that.setState({reset: false});
        });
        this.props.onClick();
        React.findDOMNode(this.refs.time).value = '';
        React.findDOMNode(this.refs.description).value = '';
        this.setState({project_id: null});
        this.setState({task_date: null});
        this.setState({id: null});
    },
    formatDate: function(timestamp) {
        var dateHelper = new DateHelper();
        return dateHelper.createUnixTimestamp(timestamp);
    },
});