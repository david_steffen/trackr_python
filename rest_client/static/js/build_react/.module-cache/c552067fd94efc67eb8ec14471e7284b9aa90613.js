var ProjectItem = React.createClass({displayName: "ProjectItem",
    render: function() {
        var divStyle = {
            backgroundColor: this.props.colour,
          };
        return (
            React.createElement("div", {onClick: this.handleClick}, 
            React.createElement("span", {className: "dropdown-colour", style: divStyle}), 
                React.createElement("span", {className: "dropdown-abr"}, this.props.abbreviation), 
                React.createElement("span", {className: "dropdown-name"}, this.props.name)
            )
        );
    },
    handleClick: function() {
        this.props.onClick(this.props.id);
    }
});
var ProjectDropdown = React.createClass({displayName: "ProjectDropdown",
    getInitialState: function() {
        return {
            projects: [],
            active: false,
            project: {colour: '#C7C7C7',abbreviation: '0' ,name: 'Please Select...'}
        };
    },
    statics: {
        initProjectValues: {colour: '#C7C7C7',abbreviation: '0' ,name: 'Please Select...'}
    },
    render: function() {
        var classes = {
            'dropdown': true,
            'active': this.state.active
        };
        var dropdownClasses = classSet(classes);
         var divStyle = {
            backgroundColor: this.state.project.colour,
          };
        var projectNodes = this.state.projects.map(function (project) {
            return React.createElement(ProjectItem, React.__spread({onClick: this.handleClick},  project))
               
        }.bind(this));

        return (
            React.createElement("ul", {"data-selected-id": this.state.id, className: dropdownClasses}, 
                React.createElement("li", {onClick: this.handleDropDown, className: "dropdown-selection"}, React.createElement("span", {className: "dropdown-colour", style: divStyle}), this.state.project.abbreviation, React.createElement("span", {className: "dropdown-name"}, " ", this.state.project.name), React.createElement("span", {className: "fa fa-chevron-down"})), 
                React.createElement("li", {className: "dropdown-wrap"}, 
                projectNodes
                )
            ) 
        );
    },
    loadProjectsFromServer: function() {
        var that = this;
        $.ajax({type: "GET",url:'/projects/listing'})
        .done(function(_data){
            if(_data.payload.success){
                that.setState({projects: _data.payload.projects});
            } else {
                that.setState({error: true});
            }
        }).fail(function(_data){
            that.setState({error: true});
            that.setState({err_msg: _data.responseJSON.payload.err_msg});
        });
    },
    componentWillReceiveProps: function(nextProps) {
        console.log(nextProps)
        for(var x = 0; x < this.state.projects.length; x++) {
            if(this.state.projects[x].id === nextProps.project_id){
                this.setState({project: this.state.projects[x]});
                break;
            }
        }
         if(nextProps.reset) {
            var  project = {colour: '#C7C7C7',abbreviation: '0' ,name: 'Please Select...'}
            this.setState({project: project});
        }
    },
    componentDidMount: function() {
        this.loadProjectsFromServer();
    },
    handleClick: function (id) {
        for(var x = 0; x < this.state.projects.length; x++) {
            if(this.state.projects[x].id === id){
                this.setState({project: this.state.projects[x].project});
                break;
            }
        }
        this.props.onClick(id);
        this.handleDropDown();
    },
    handleDropDown: function(e) {
        if(this.state.active) {
            this.setState({active: false});
        } else {
            this.setState({active: true});
        }
    }
});