var TaskForm = React.createClass({displayName: "TaskForm",
    getInitialState: function() {
        return {
            project_id: null,
            task_date: null,
            id: null,
            reset: false,
        };
    },
    componentDidMount: function() {
        var dateObj = new Date();
        var dateHelper = new DateHelper();
        dateHelper.setDateObject(dateObj);
        this.setState({task_date: dateHelper.getTimestampString()});
    },
    componentWillReceiveProps: function(nextProps) {
        var task = nextProps.task
        if(typeof task !== 'undefined' && nextProps.update) {
            React.findDOMNode(this.refs.time).value = task.time,
            React.findDOMNode(this.refs.description).value = task.description,
            this.setState({project_id: task.project_id});
            this.setState({task_date: task.task_date});
            this.setState({id: task.id});
        }
    },
    render: function() {
        var classes = {
            'add-item': true,
            'open': this.props.open
        };
        var formClasses = classSet(classes);
        return (

            React.createElement("form", {onSubmit: this.handleSubmitForm}, 
                React.createElement("ul", {className: formClasses}, 
                    React.createElement("li", null, 
                        React.createElement("label", {for: "time"}, "Time:"), 
                        React.createElement("input", {placeholder: "0.0", className: "form-input short", type: "text", ref: "time", name: "time", onChange: this.handleChange})
                    ), 
                    React.createElement("li", null, 
                        React.createElement("label", {for: "description"}, "Description:"), 
                        React.createElement("input", {placeholder: "Add description here", className: "form-input long", ref: "description", type: "text", name: "description", onChange: this.handleChange})
                    ), 
                    React.createElement("li", null, 
                        React.createElement("label", null, "Project:"), 
                            React.createElement(ProjectDropdown, {reset: this.state.reset, project_id: this.state.project_id, onClick: this.handleClick})
                    ), 
                    React.createElement("li", null, 
                        React.createElement("label", null, "Date:"), 
                        React.createElement(DayPicker, {reset: this.state.reset, task_date: this.state.task_date, onClick: this.handleDate})
                    ), 
                    React.createElement("li", {className: "form-buttons-wrap"}, 
                        React.createElement("div", {className: "button"}, React.createElement("input", {onClick: this.cancelForm, value: "Cancel", name: "cancel", type: "button"}), React.createElement("input", {value: "Submit", name: "log", type: "submit"}))
                    )
              )
          )
        );
    },
    updateTaskState: function(key, value) {
//        var input= (this.state.task)? this.state.task: {task:{}};
//        input[ key ] = value;
//        this.setState(input);
    },
    handleClick: function (id) {
        this.setState({project_id: id});
    },
    handleChange: function(event) {
        //this.updateTaskState(event.target.name, event.target.value);
        //this.props.onChange(event)
    },
    handleSubmitForm: function(e) {
        e.preventDefault();
        var dateHelper = new DateHelper();
        var date = (dateHelper.isJSTimestamp(this.state.task_date)) ? dateHelper.createUnixTimestamp(this.state.task_date): this.state.task_date;
        var data = {
            id: this.state.id,
            time: React.findDOMNode(this.refs.time).value,
            description: React.findDOMNode(this.refs.description).value,
            project_id: this.state.project_id,
            task_date: date
        }
        this.props.onSubmit(data);
    },
    handleDate: function (date) {
        this.updateTaskState('task_date', date.timestamp);
    },
    cancelForm: function() {
        var that =this;
        this.setState({reset: true},function() {
            that.setState({reset: false});
        });
        this.props.onClick(false);
        React.findDOMNode(this.refs.time).value = '';
        React.findDOMNode(this.refs.description).value = '';
        this.setState({project_id: null});
        this.setState({task_date: null});
        this.setState({id: null});
    },
    formatDate: function(timestamp) {
        var dateHelper = new DateHelper();
        return dateHelper.createUnixTimestamp(timestamp);
    },
});