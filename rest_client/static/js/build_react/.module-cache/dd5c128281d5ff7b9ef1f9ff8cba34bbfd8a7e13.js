var ProjectForm = React.createClass({displayName: "ProjectForm",
    getInitialState: function() {
        return {
            id: null,
            colour: null,
            reset: false,
        };
    },
    componentDidMount: function() {
        var that = this;
        $(this.getDOMNode()).find('input.colour').colorPicker({customBG: '#ffffff', cssAddons:'.cp-color-picker {z-index:5;}'});
    },
    componentWillReceiveProps: function(nextProps) {
        var project = nextProps.project
        if(typeof project !== 'undefined' && nextProps.update) {
            React.findDOMNode(this.refs.colour).value = project.colour,
            React.findDOMNode(this.refs.abbreviation).value = project.abbreviation,
            React.findDOMNode(this.refs.name).value = project.name,
            $(this.getDOMNode()).find('input.colour').colorPicker({customBG: project.colour, color:'rgba(0, 0, 0, 0)'});
            this.setState({id:project.id});
        }
    },
    componentDidUpdate: function(props){
        console.log(props);
    },
    render: function() {
        var classes = {
            'add-item': true,
            'open': this.props.open
        };
        var formClasses = classSet(classes);
        return (
            React.createElement("form", {onSubmit: this.handleSubmitForm}, 
                React.createElement("ul", {className: formClasses}, 
                    React.createElement("li", null, 
                        React.createElement("label", {for: "colour"}, "Colour:"), 
                        React.createElement("input", {placeholder: "#FFFFFF", defaultValue: "#FFFFFF", className: "colour form-input", type: "text", ref: "colour", name: "colour", onChange: this.handleChange})
                    ), 
                    React.createElement("li", null, 
                        React.createElement("label", {for: "abbreviation"}, "Abbreviation:"), 
                        React.createElement("input", {placeholder: "Should be roughly 3 letter eg. ABR", className: "form-input", ref: "abbreviation", type: "text", name: "abbreviation", onChange: this.handleChange})
                    ), 
                    React.createElement("li", null, 
                        React.createElement("label", {for: "name"}, "Name:"), 
                        React.createElement("input", {placeholder: "Name of project", className: "form-input", ref: "name", type: "text", name: "name", onChange: this.handleChange})
                    ), 
                    
                    React.createElement("li", {className: "form-buttons-wrap"}, 
                        React.createElement("div", {className: "button"}, React.createElement("input", {onClick: this.cancelForm, value: "Cancel", name: "cancel", type: "button"}), React.createElement("input", {value: "Submit", name: "log", type: "submit"}))
                    )
              )
          )
        );
    },
    updateProjectState: function(key, value) {
//        var input= (this.state.project)? this.state.project: {project:{}};
//        input[ key ] = value;
//        this.setState(input);
    },

    handleChange: function(event) {
        console.log(event.target);
    },
    handleSubmitForm: function(e) {
        e.preventDefault();
        var data = {
            id: this.state.id,
            colour: React.findDOMNode(this.refs.colour).value,
            abbreviation: React.findDOMNode(this.refs.abbreviation).value,
            name: React.findDOMNode(this.refs.name).value,
        }
        this.props.onSubmit(data);
    },
    handleDate: function (date) {
        this.updateTaskState('task_date', date.timestamp);
    },
    cancelForm: function() {
        this.props.onClick();
        this.setState({id:''});
        var that =this;
        this.setState({reset: true},function() {
            that.setState({reset: false});
        })
        React.findDOMNode(this.refs.colour).value = '#FFFFFF';
        React.findDOMNode(this.refs.abbreviation).value = '';
        React.findDOMNode(this.refs.name).value = '';
        $(this.getDOMNode()).find('input.colour').colorPicker({customBG: '#ffffff' });
    },
    formatDate: function(timestamp) {
        var dateHelper = new DateHelper();
        return dateHelper.createUnixTimestamp(timestamp);
    },
});