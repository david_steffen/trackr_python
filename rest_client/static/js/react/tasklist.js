var TaskRow  = React.createClass({
    getInitialState: function() {
      return {
          edit: false,
      }  
    },
    render: function() {
        var dateHelper = new DateHelper();
        var t_stamp = dateHelper.createJSTimestamp(this.props.task.task_date);
        var date = new Date(t_stamp);
        dateHelper.setDateObject(date);
        var formattedDate =  dateHelper.humanReadible();
        var classes = {
            'list-row': true,
            'edit': this.state.edit
        };
        var editableClasses = classSet(classes);
        var divStyle = {
            backgroundColor: this.props.task.colour,
        };
        
        return (
            <ul data-id={this.props.task.id} className={editableClasses} >
                <li className='list-item list-date' ref='date'>{formattedDate}</li>
                <li className='list-item list-time' ref='time'><span className="fa fa-clock-o"></span>{this.props.task.time}</li>
                <li className='list-item list-abr' ref='project'><div className="project-colour" style={divStyle}></div>{this.props.task.abr}</li>
                <li className='list-item list-description' ref='description'>{this.props.task.description}</li>
                <li className='list-item list-edit'><span onClick={this.handleClick} className='fa fa-pencil-square'></span></li>
            </ul>
        );
    },
    handleClick: function(e) {
        //this.setState({edit:true})
        this.props.onClick(this.props.task.id)
//        React.findDOMNode(this.refs.time).contentEditable = true;
//        React.findDOMNode(this.refs.description).contentEditable = true;
    },
});

var TaskList = React.createClass({
    getInitialState: function() {
      return {
          edit: false,
      }  
    },
    render: function() {
        var classes = {
            'edit-wrap': true,
            'edit': this.state.edit
        };
        var editableClasses = classSet(classes);
        return (
            <ReactCSSTransitionGroup transitionName='list-table'>
            {this.props.tasks.map(function (task) {
                   return <TaskRow onClick={this.handleClick} key={task.task.id} task={task.task} />
                }.bind(this))}
            </ReactCSSTransitionGroup>
        );
    },
    handleClick: function(id) {
        this.setState({edit: true})
        this.props.onClick(id)
    },
});