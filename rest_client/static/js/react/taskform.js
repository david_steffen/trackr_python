var TaskForm = React.createClass({
    getInitialState: function() {
        return {
            project_id: null,
            task_date: null,
            id: null,
            reset: false,
        };
    },
    componentDidMount: function() {
        var dateObj = new Date();
        var dateHelper = new DateHelper();
        dateHelper.setDateObject(dateObj);
        this.setState({task_date: dateHelper.getTimestampString()});
    },
    componentWillReceiveProps: function(nextProps) {
        var task = nextProps.task
        if(typeof task !== 'undefined' && nextProps.update) {
            React.findDOMNode(this.refs.time).value = task.time,
            React.findDOMNode(this.refs.description).value = task.description,
            this.setState({project_id: task.project_id});
            this.setState({task_date: task.task_date});
            this.setState({id: task.id});
        }
    },
    render: function() {
        var classes = {
            'add-item': true,
            'open': this.props.open
        };
        var formClasses = classSet(classes);
        return (

            <form onSubmit={this.handleSubmitForm}>
                <ul className={formClasses}>
                    <li>
                        <label for="time">Time:</label>
                        <input placeholder='0.0'  className='form-input short' type="text" ref="time" name="time" onChange={this.handleChange}/>
                    </li>
                    <li>
                        <label for="description">Description:</label>
                        <input placeholder='Add description here'  className='form-input long' ref="description" type="text" name="description" onChange={this.handleChange}/>
                    </li>
                    <li>
                        <label>Project:</label>
                            <ProjectDropdown reset={this.state.reset} project_id={this.state.project_id} onClick={this.handleClick} />
                    </li>
                    <li>
                        <label>Date:</label>
                        <DayPicker reset={this.state.reset} task_date={this.state.task_date} onClick={this.handleDate} />
                    </li>
                    <li className="form-buttons-wrap">
                        <div className="button"><input onClick={this.cancelForm} value="Cancel" name="cancel" type="button"/><input  value="Submit" name="log" type="submit"/></div>
                    </li>
              </ul>
          </form>
        );
    },
    updateTaskState: function(key, value) {
//        var input= (this.state.task)? this.state.task: {task:{}};
//        input[ key ] = value;
//        this.setState(input);
    },
    handleClick: function (id) {
        this.setState({project_id: id});
    },
    handleChange: function(event) {
        //this.updateTaskState(event.target.name, event.target.value);
        //this.props.onChange(event)
    },
    handleSubmitForm: function(e) {
        e.preventDefault();
        var dateHelper = new DateHelper();
        var date = (dateHelper.isJSTimestamp(this.state.task_date)) ? dateHelper.createUnixTimestamp(this.state.task_date): this.state.task_date;
        var data = {
            id: this.state.id,
            time: React.findDOMNode(this.refs.time).value,
            description: React.findDOMNode(this.refs.description).value,
            project_id: this.state.project_id,
            task_date: date
        }
        this.props.onSubmit(data);
    },
    handleDate: function (date) {
        this.updateTaskState('task_date', date.timestamp);
    },
    cancelForm: function() {
        this.props.onClick();
        this.setState({project_id: null});
        this.setState({task_date: null});
        this.setState({id: null});
        var that =this;
        this.setState({reset: true},function() {
            that.setState({reset: false});
        })
        React.findDOMNode(this.refs.time).value = '';
        React.findDOMNode(this.refs.description).value = '';
    },
    formatDate: function(timestamp) {
        var dateHelper = new DateHelper();
        return dateHelper.createUnixTimestamp(timestamp);
    },
});