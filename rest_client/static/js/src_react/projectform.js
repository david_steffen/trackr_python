var ProjectForm = React.createClass({
    getInitialState: function() {
        return {
            id: null,
            colour: null,
            reset: false,
        };
    },
    componentDidMount: function() {
        var that = this;
        $(this.getDOMNode()).find('input.colour').colorPicker({
            customBG: '#ffffff',
            cssAddon: 
                '.cp-disp{padding:10px; margin-bottom:6px; font-size:19px; height:20px; line-height:20px}' +
                '.cp-xy-slider{width:200px; height:200px;}' +
                '.cp-xy-cursor{width:16px; height:16px; border-width:2px; margin:-8px}' +
                '.cp-z-slider{height:200px; width:40px;}' +
                '.cp-z-cursor{border-width:8px; margin-top:-8px;}' +
                '.cp-alpha{height:40px;}' +
                '.cp-alpha-cursor{border-width: 8px; margin-left:-8px;}',
        });
    },
    componentWillReceiveProps: function(nextProps) {
        var project = nextProps.project
        if(typeof project !== 'undefined' && nextProps.update) {
            React.findDOMNode(this.refs.colour).value = project.colour;
            React.findDOMNode(this.refs.abbreviation).value = project.abbreviation;
            React.findDOMNode(this.refs.name).value = project.name;
            this.setState({id:project.id});
            $(this.getDOMNode()).find('input.colour').colorPicker({customBG: project.colour, color:'rgba(0, 0, 0, 0)'});
        }
        
    },
    render: function() {
        var classes = {
            'add-item': true,
            'open': this.props.open
        };
        var formClasses = classSet(classes);
        return (
            <form onSubmit={this.handleSubmitForm}>
                <ul className={formClasses}>
                    <li>
                        <label for="colour">Colour:</label>
                        <input placeholder='#FFFFFF' defaultValue='#FFFFFF' className='colour form-input' type="text" ref="colour" name="colour" onChange={this.handleChange}/>
                    </li>
                    <li>
                        <label for="abbreviation">Abbreviation:</label>
                        <input placeholder='Max 6 letters eg. ABR'  className='form-input short uppercase' ref="abbreviation" type="text" name="abbreviation" onChange={this.handleChange}/>
                    </li>
                    <li>
                        <label for="name">Name:</label>
                        <input placeholder='Name of project'  className='form-input' ref="name" type="text" name="name" onChange={this.handleChange}/>
                    </li>
                    
                    <li className="form-buttons-wrap">
                        <div className="button"><input onClick={this.cancelForm} value="Cancel" name="cancel" type="button"/><input  value="Submit" name="log" type="submit"/></div>
                    </li>
              </ul>
          </form>
        );
    },
    updateProjectState: function(key, value) {
//        var input= (this.state.project)? this.state.project: {project:{}};
//        input[ key ] = value;
//        this.setState(input);
    },

    handleChange: function(event) {
        //console.log(event.target);
    },
    handleSubmitForm: function(e) {
        e.preventDefault();
        var data = {
            id: this.state.id,
            colour: React.findDOMNode(this.refs.colour).value,
            abbreviation: React.findDOMNode(this.refs.abbreviation).value,
            name: React.findDOMNode(this.refs.name).value,
        }
        this.props.onSubmit(data);
    },
    cancelForm: function() {
        this.props.onClick();
        this.setState({id:null});
        var that =this;
        this.setState({reset: true},function() {
            that.setState({reset: false});
        })
        React.findDOMNode(this.refs.colour).value = '#FFFFFF';
        React.findDOMNode(this.refs.abbreviation).value = '';
        React.findDOMNode(this.refs.name).value = '';
        $(this.getDOMNode()).find('input.colour').colorPicker({customBG: '#ffffff' });
    },
    formatDate: function(timestamp) {
        var dateHelper = new DateHelper();
        return dateHelper.createUnixTimestamp(timestamp);
    },
});