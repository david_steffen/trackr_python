
var ProjectRow  = React.createClass({
    getInitialState: function() {
      return {
          edit: false,
      }  
    },
    render: function() {
//        var dateHelper = new DateHelper();
//        var t_stamp = dateHelper.createJSTimestamp(this.props.task.task_date);
//        var date = new Date(t_stamp);
//        dateHelper.setDateObject(date);
//        var formattedDate =  dateHelper.humanReadible();
        var classes = {
            'list-row': true,
            'edit': this.state.edit
        };
        var editableClasses = classSet(classes);
        var total_time = this.calcTimes(this.props.project.total_times)
        var divStyle = {
            backgroundColor: this.props.project.colour,
          };
        return (

                <ul data-id={this.props.project.id} data-total={total_time} className={editableClasses}>
                {/*<li className='project-item-date' ref='date'>{formattedDate}</li>
                    <li className='list-item list-colour' ref='colour'><div className="project-colour" style={divStyle}></div></li>*/}
                    <li className='list-item list-abr' ref='abbreviation'><div className="project-colour" style={divStyle}></div>{this.props.project.abbreviation}</li>
                    <li className='list-item list-name' ref='name'>{this.props.project.name}</li>
                    <li className='list-item list-total' ref='total-time'>{total_time}</li>
                    <li className='list-item list-edit'><span onClick={this.handleClick} className='fa fa-pencil-square'></span></li>
                </ul>

        );
    },
    calcTimes: function(times) {
        if(!times) { return 0; }
        var total_time = 0;
        for(x in times) {
            total_time += parseFloat(times[x].time);
        }
        return total_time;
    },
    handleClick: function(e) {
        this.props.onClick(this.props.project.id)
    },
});

var ProjectList = React.createClass({
    getInitialState: function() {
      return {
          edit: false,
      }  
    },
    render: function() {
        var classes = {
            'edit-wrap': true,
            'edit': this.state.edit
        };
        var editableClasses = classSet(classes);
        return (
            <ReactCSSTransitionGroup transitionName='list-table'>
            {this.props.projects.map(function (project) {
                   return <ProjectRow onClick={this.handleClick} key={project.project.id} project={project.project} />
                }.bind(this))}
            </ReactCSSTransitionGroup>
        );
    },
    handleClick: function(id) {
        this.setState({edit: true})
        this.props.onClick(id)
    },
});