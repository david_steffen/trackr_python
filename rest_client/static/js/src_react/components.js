//var cx = React.addons.classSet;

var DayItem = React.createClass({
    getInitialState: function() {
        return {
            selected: false,
        };
    },
    render: function() {
        var dateNodes = this.props.data.map(function (date,x) {
            var dateObj = new Date();
            var dateHelper = new DateHelper();
            dateHelper.setDateObject(dateObj);
            var currentDate = dateHelper.humanReadible();
            if(currentDate === date.formattedDate){
            var dateClasses = 'day-picker selected'
            } else {
               var dateClasses =  'day-picker'
            }
            return (
                <div key={x} data-date={date.timestamp} onClick={this.handleDate} className={dateClasses}>{date.formattedDate}</div>
            );
        }.bind(this));
        return (
            <div className="day-picker-inner-wrap">
                {dateNodes}
            </div>
        );
    },
    handleDate: function(e) {
        $(e.target).addClass('selected').siblings().removeClass('selected');
        this.props.onClick(e.target.dataset.date);
    },
});
var DayPicker = React.createClass({
    getInitialState: function() {
        return {
            error: false,
            err_msg: "",
            data: [],
            lastDate: '',
            currentDate: ''
        };
    },
    render: function() {
        return (
            <div className="day-picker-wrap">
                <div onClick={this.handlePreviousDates} className='day-picker date-prev fa fa-chevron-left'></div>
                <DayItem onClick={this.handleDate} data={this.state.data} />
                <div onClick={this.handleNextDates} className='day-picker date-next fa fa-chevron-right'></div>
            </div>
        );
    },
    componentDidMount: function() {
        var dateObj = new Date();
        var dateHelper = new DateHelper();
        dateHelper.setDateObject(dateObj);
        this.setState({currentDate: dateHelper.getTimestampString()});
        var datesArray =  dateHelper.getPreviousDates(dateObj, 3);
        this.setState({data: datesArray});
        var lastDate = dateHelper.getTimestampString();
        this.setState({lastDate: lastDate});
    },
    handlePreviousDates: function(e) {
        this.removeSelectedClass(e.target)
        var dateObj = new Date(this.state.lastDate);
        var dateHelper = new DateHelper();
        dateHelper.setDateObject(dateObj);
        this.setState({currentDate: dateHelper.getTimestampString()});
        var datesArray =  dateHelper.getPreviousDates(dateObj, 3);
        this.setState({data: datesArray});
        var lastDate = dateHelper.getTimestampString();
        this.setState({lastDate: lastDate});
    },
    removeSelectedClass: function(el) {
        $(el).siblings('.day-picker-inner-wrap').find('.selected').removeClass('selected');
    },
    handleNextDates: function(e) {
        this.removeSelectedClass(e.target)
        var dateObj = new Date(this.state.currentDate);
        var dateHelper = new DateHelper();
        dateHelper.setDateObject(dateObj);
        this.setState({lastDate: dateHelper.getTimestampString()});
        var datesArray =  dateHelper.getNextDates(dateObj, 3);
        this.setState({data: datesArray});
        var currentDate = dateHelper.getTimestampString();
        this.setState({currentDate: currentDate});
    },
    handleDate: function (date) {
        this.setState({date: date});
        this.props.onClick(date);
    },
});
var ProjectRow = React.createClass({
    render: function() {
        return (
            <div onClick={this.handleClick} data-id={this.props.id} data-abr={this.props.abbreviation}>
                <span className="project-item-abr" >{this.props.abbreviation}</span>
                <span className="project-item-name" >{this.props.name}</span>
            </div>
        );
    },
    handleClick: function(e) {
        var el = this.getDOMNode();
        this.props.onClick(el.dataset.id, el.dataset.abr);
    }
});
var ProjectList = React.createClass({
    getInitialState: function() {
        return {
            error: false,
            err_msg: "",
            data: [],
            id: '',
            active: false,
            activeClass: 'active',
            project: ''
        };
    },
    render: function() {
        var classes = {
            'project-list': true,
            'active': this.state.active
        };
        var dropdownClasses = cx(classes);
        var selectedOption = (this.state.id) ? this.state.project : 'Please select...';
        var projectNodes = this.state.data.map(function (project) {
            return (
                <div key={project.id}>
                    <ProjectRow onClick={this.handleClick} {...project} />
                </div>
            );
        }.bind(this));
        return (
            <ul data-selected-id={this.state.id} className={dropdownClasses}>
                <li onClick={this.handleDropDown} className='project-list-selection'>{selectedOption}<span className="fa fa-chevron-down"></span></li>
                <li className="project-list-wrap">
                {projectNodes}
                </li>
            </ul> 
        );
    },
    loadProjectsFromServer: function() {
        var that = this;
        $.ajax({type: "GET",url:'/projects/view'})
        .done(function(_data){
            if(_data.payload.success){
                that.setState({data: _data.payload.projects});
            } else {
                that.setState({error: true});
            }
        }).fail(function(_data){
            that.setState({error: true});
            that.setState({err_msg: _data.responseJSON.payload.err_msg});
        });
    },
    componentDidMount: function() {
        this.loadProjectsFromServer();
    },
    handleClick: function (id,project) {
        this.props.onClick(id);
        this.setState({id: id});
        this.setState({project: project});
        this.handleDropDown();
    },
    handleDropDown: function(e) {
        if(this.state.active) {
            this.setState({active: false});
        } else {
            this.setState({active: true});
        }
    }
});
var TaskForm = React.createClass({
    getInitialState: function() {
        return {
            time: '',
            description: '',
            project_id: '',
            date: '',
        };
    },
    render: function() {
        return (
            <form onSubmit={this.handleSubmitForm}>
                <ul className='add-task'>
                    <li>
                        <label>Project:</label>
                            <ProjectList onClick={this.handleClick} />
                    </li>
                    <li>
                        <label for="time">Time:</label>
                        <input placeholder='0.0' className='form-input' type="text" ref="time" name="time" onChange={this.handleChange}/>
                    </li>
                    <li>
                        <label for="description">Description:</label>
                        <input placeholder='Add description here' className='form-input' ref="description" type="text" name="description" onChange={this.handleChange}/>
                    </li> 
                    <li>
                        <label>Date:</label>
                        <DayPicker onClick={this.handleDate} />
                    </li>
                    <li>
                        <div className="button"><input value="Add task" name="log" type="submit"/></div>
                    </li>
              </ul>
          </form>
        );
    },
    handleClick: function (id) {
        this.setState({project_id: id});
    },
    handleChange: function(event) {
        var input = {}
        input[ event.target.name ] = event.target.value
        this.setState(input);
    },
    handleSubmitForm: function(e) {
        e.preventDefault();
        this.props.onSubmit(this);
    },
    handleDate: function (date) {
        this.setState({date: date});
    },
    clearFormInputs: function() {
        React.findDOMNode(this.refs.time).value = '';
        React.findDOMNode(this.refs.description).value = '';
    },
});



