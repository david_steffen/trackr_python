var Login = React.createClass({

  getInitialState: function() {
    return {
      email: "",
      password: ""
    };
  },


  render: function() {
    return (
        <form onSubmit={this.handleSubmitForm}>
            <div>
                <div>
                    <label for="email">Email<span class="required">*</span></label>
                    <input type="text" name="email" ref="email"/>
                </div>
                <div>
                    <label for="password">Password<span class="required">*</span></label>
                    <input type="password" name="password" ref="password"/>
                </div>   
                <div>
                    <div class="button"> <input value="Login" name="login" type="submit"/></div>
                </div>
            </div>
        </form>
    );
  },

  handleSubmitForm: function(e) {
    e.preventDefault();
    var email = React.findDOMNode(this.refs.email).value.trim();
    var password = React.findDOMNode(this.refs.password).value.trim();
    console.log(email);
    $.ajax({type: "POST",url:'/users/login',data: {email: email, password:password}})
                .done(function(_data){
            if(_data.logged_in) {
                window.location.href = '/tracker'
            }
        });

  }
});
if(document.getElementById('login')) {
React.render(
    <Login />,
    document.getElementById('login')
  );
}