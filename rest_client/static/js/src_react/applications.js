var Projects = React.createClass({
    mixins: [FluxMixin, StoreWatchMixin("ProjectStore")],
    getInitialState: function() {
        return {
            projectToEdit: [],
            update: false,
            fixed: false,
            openForm: false,
        };
    },
    getStateFromFlux: function() {
        var store = this.getFlux().store("ProjectStore");
        return {
          loading: store.loading,
          error: store.error,
          projects: _.values(store.projects),
          success: store.success,
        };
    },
    render: function() {
        var formHeading = (this.state.update) ? 'Edit project' : 'Add a project';
        var classes = {
            'form-wrap': true,
            'fixed': this.state.fixed
        };
        var formClasses = classSet(classes);
        var classes = {
            'list-wrap': true,
        };
        var listClasses = classSet(classes);
        return (
            <div className='content-wrap'>
                <div className={formClasses}>
                    <h1 onClick={this.toggleForm} className='form-title'><span className="fa fa-chevron-right"></span>{formHeading}</h1>
                    <ProjectForm onClick={this.cancelForm} open={this.state.openForm} onSubmit={this.handleSubmit} update={this.state.update} project={this.state.projectToEdit} />
                </div>
                <div className='list-wrap'>
                    <h1 className='list-title'><span className="fa fa-chevron-right"></span>Projects</h1>
                    {this.state.error ? "<span>Error loading data</span>" : null}
                    {this.state.loading ? <span>Loading...</span> : null}
                    <div className='list-table'>
                        <ProjectList projects={this.state.projects} onClick={this.handleEditProject} />
                    </div>
                </div>
                <div className='project-stats-wrap'>
                
                </div>
            </div>
        );
    },
    handleSubmit: function(props) {
        (this.state.update) ? this.getFlux().actions.projects.updateProject(props) : this.getFlux().actions.projects.submitProject(props);
        if(this.state.success) {
            this.setState({update: false});
        }
    },
    handleEditProject: function(id) {
        if(this.state.update) {
            return null;
        }
        this.setState({update: true});
        this.setState({openForm: true});
        for(var x = 0; x < this.state.projects.length; x++) {
            if(this.state.projects[x].id === id) {
                var project = this.state.projects[x].project;
                this.setState({projectToEdit: project});
                break;
            }
            
        }
    },
    toggleForm: function() {
        if(this.state.openForm) {
            this.setState({openForm: false});
        } else {
            this.setState({openForm: true});
        }
    },
    cancelForm: function() {
        this.setState({openForm: false});
        this.setState({update: false});
    },
    componentDidMount: function() {
        this.getFlux().actions.parojects.projects.loadProject();
        var top = $('.form-wrap').offset().top - parseFloat($('.form-wrap').css('margin-top').replace(/auto/, 0));
        var that = this;
        $(window).scroll(function (event) {
            // what the y position of the scroll is
            var y = $(this).scrollTop();
            // whether that's below the form
            if (y >= top) {
                // if so, ad the fixed class
                that.setState({fixed: true});
            } else {
                // otherwise remove it
                that.setState({fixed: false});
            }
        });
    },
});

var Tracker = React.createClass({
    initTask: {id : null, time : null, description : null, project_id :null, task_date :null},
    mixins: [FluxMixin, StoreWatchMixin("TaskStore")],
    getInitialState: function() {
        return {
            taskToEdit: this.initTask,
            update: false,
            fixed: false,
            openForm: false,
            reset: false,
        };
    },
    getStateFromFlux: function() {
        var store = this.getFlux().store("TaskStore");
        return {
          loading: store.loading,
          error: store.error,
          tasks: _.values(store.tasks).reverse(),
          success: store.success,
        };
    },
    componentDidMount: function() {
        this.getFlux().actions.tasks.loadTask();
        var top = $('.form-wrap').offset().top - parseFloat($('.form-wrap').css('margin-top').replace(/auto/, 0));
        var that = this;
        $(window).scroll(function (event) {
            // what the y position of the scroll is
            var y = $(this).scrollTop();
            // whether that's below the form
            if (y >= top) {
                // if so, ad the fixed class
                that.setState({fixed: true});
            } else {
                // otherwise remove it
                that.setState({fixed: false});
            }
        });
//        var dateObj = new Date();
//        var dateHelper = new DateHelper();
//        dateHelper.setDateObject(dateObj);
//        this.updateTaskState('task_date', dateHelper.getTimestampString());
    },
    render: function() {
        var formHeading = (this.state.update) ? 'Edit task' : 'Add a task';
        var classes = {
            'form-wrap': true,
            'fixed': this.state.fixed
        };
        var formClasses = classSet(classes);
        
       
        var deleteButton = (this.state.update) ?  <FormButton value='Delete' type='button' name='delete' onClick={this.deleteTask}/>: null;
        return (
            <div className='content-wrap'>
                <div className={formClasses}>
                                <div className='form-inner-wrap'>
                    <h1 onClick={this.toggleForm} className='form-title'><span className="fa fa-chevron-right"></span>{formHeading}</h1>
                    <TaskForm onSubmit={this.handleSubmit} open={this.state.openForm} update={this.state.update} reset={this.state.reset} task={this.state.taskToEdit}>
                        {deleteButton}
                        <FormButton classes='right' value='Cancel' type='button' name='cancel' onClick={this.cancelForm} />
                        <FormButton classes='right' value='Submit' type='submit' name='log' />
                    </TaskForm>
                    </div>
                </div>
                <div className='list-wrap'>
                    <h1 className='list-title'><span className="fa fa-chevron-right"></span>Tasks</h1>
                    {this.state.error ? "<span>Error loading data</span>" : null}
                    {this.state.loading ? <span>Loading...</span> : null}
                    <div className='list-table'>
                        <TaskList tasks={this.state.tasks} onClick={this.handleEditTask} />
                    </div>
                </div>
            </div>
        );
    },
    toggleForm: function() {
        if(this.state.openForm) {
            this.setState({openForm: false});
        } else {
            this.setState({openForm: true});
        }
    },
    handleSubmit: function(data) {
//        var dateHelper = new DateHelper();
//        if(this.state.taskToEdit.task_date) {
//            var date = (dateHelper.isJSTimestamp(this.state.taskToEdit.task_date)) ? dateHelper.createUnixTimestamp(this.state.taskToEdit.task_date): this.state.taskToEdit.task_date;
//        } else {
//            var date = dateHelper.createUnixTimestamp(new Date());
//        }
//        var task = {};
//        task.id = this.state.taskToEdit.id;
//        task.time = this.state.taskToEdit.time;
//        task.description = this.state.taskToEdit.description;
//        task.project_id = this.state.taskToEdit.project_id;
//        task.task_date = date;
        
        (this.state.update) ? this.getFlux().actions.tasks.updateTask(data) : this.getFlux().actions.tasks.addTask(data);
    },
    componentWillUpdate: function(nextProps, nextState) {
//        var task = nextState.taskToEdit
//        if(typeof task !== 'undefined' && nextProps.update) {
//            React.findDOMNode(this.refs.time).value = task.time;
//            React.findDOMNode(this.refs.description).value = task.description;
//        }

        if(nextState.success) {
            this.cancelForm();
        }
        if(nextState.reset) {
            this.setState({reset: false});
        }
    },
    componentDidUpdate: function(prevProps, prevState) {
        console.log(prevState)
    
        if(prevState.success) {
            this.setState({success: false});
        }
    },
    handleEditTask: function(id) {
        if(this.state.update) {
            return null;
        }
        this.setState({openForm: true,update: true});
        for(var x = 0; x < this.state.tasks.length; x++) {
            if(this.state.tasks[x].id === id) {
                var task = JSON.parse(JSON.stringify(this.state.tasks[x].task));
                this.setState({taskToEdit: task});
                break;
            }
            
        }
    },
    cancelForm: function() {
        this.setState({reset: true});
        var that =this;
        this.setState({update: false,taskToEdit:  this.initTask, openForm: false});
        
    },
  
});





if(document.getElementById('tracker')) {   
    React.render(
        <Tracker flux={flux} />,
        document.getElementById('tracker')
    );
}

if(document.getElementById('projects')) {   
    React.render(
        <Projects flux={flux} />,
        document.getElementById('projects')
    );
}

