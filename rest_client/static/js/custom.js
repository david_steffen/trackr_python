//
//
//
//function PaginateJobs() {
//
//}
//
//PaginateJobs.prototype.post = function() {
//  
//$('#paginate').on("click",".page", function(event) {
//    event.preventDefault();
//    var link = $(this).attr('href');
//    var equalPosition = link.indexOf('='); //Get the position of '='
//    var number = link.substring(equalPosition + 1);
//var url = getUrl()+"postscripts/jobList_paginate.php";
//    $.post(url, {page_no : number}, function(data) {
//        var pgtxt = $(data).find( '#pgtxt' );
//        var pagination = $(data).find( '.page' );
//        $('#paginate').empty(this).append(pgtxt).append(pagination);
//            var jobs = $(data).find('.post');
//     $('#joblist').empty(this).append(jobs);
//    }).error(function(data) {alert(data);});
//});
//}
var replaceNullValue = function(str) 
{
    	var trimstr = $.trim(str);
            if(trimstr === null || trimstr === '' || trimstr === 'null null') {
                return 'n/a';
            } else {
                return trimstr;
            } 
};

var formatDate = function (str) 
{
    	var t = str.split(/[- :]/);
    	var date = new Date(t[0], t[1] - 1, t[2], t[3], t[4], t[5]);
    	var formatDate = date.getDate()+'/'+(parseInt(date.getMonth())+1)+'/'+date.getFullYear();
    	return formatDate;
};


//Base class for dealing with lists
var ListBase = function(listType) 
{
       this.id = 0,
       this.dataUrl,
       this.addDataUrl,
       this.updateDataUrl,
       this.data = null,
       this.listType = listType;
       this.init();
};
    ListBase.prototype.init = function()
    {
        var that = this;
        $('#list .listed-item').on("click", function() {
            that.listAction($(this));
        });
        $('#list_form_wrap').find('.save-list').on('click',function() {
            that.getFormData();
            that.saveList();
        });
        $('#detail_options').find('.list-form-tab').on('click',function() {
            that.switchTab($(this));
        });
        $(window).on('keyup',function(e) {
            if(e.keyCode === 13 && that.getFormData() && !($('#option_display').hasClass('selected'))) {
                that.saveList();
            }
        })
        this.switchTab($('#option_add')); 
    };
    ListBase.prototype.setId = function(id) 
    {
        this.id = id;
    };
    ListBase.prototype.getId = function() 
    {
      return this.id;  
    };
    ListBase.prototype.listAction = function(el) 
    {
        var that = this;
        var id = parseInt(el.attr('data-id'));
        el.addClass('selected').siblings().removeClass('selected');
        if(id !==  that.id) {
            $('#side-content').addClass('loading').find('.list-entry-item').addClass('hidden');
            that.setId(id);
            that.getListData().done(function(_data) {
                if(_data.payload.success){
                    that.cacheData(_data);
                    that.displayList();
                }
               }).fail(function(_response){
                    if(!parseInt(_response.responseJSON.logged_in)) { 
                        window.location.reload(); 
                    } else {
                        alert(_response.responseJSON.payload.err_msg);
                    }
            });;
        }
    };
    ListBase.prototype.getFormData = function() 
    {
        var scope = $('#list_form_wrap');
        if(!this.getId()) {
            this.data = {};
        }
        
        var el = scope.find('.list-form-item input, .list-form-item textarea, .list-form-item select');
        var flag = false;
        for(var x = 0;x <= el.length;x++) {
            var name = $(el[x]).attr('name');
            if(typeof name !== 'undefined') {
                var el_val = $.trim($(el[x]).val());
                if(name === 'job_title' && el_val === '') {
                    alert('Please enter the job title');
                    return;
                }
                if(el_val !== '') {
                    flag = true;
                }
                if (this.data[ name ] !== el_val) {
                    this.data[ name ]= el_val;
                }
            }
        }
        return flag;
    };
    ListBase.prototype.cacheData = function(data) 
    {
        var _data;
        switch(this.listType){
            case 'jobs': _data = data.payload.jobs[0];
                break;
            case 'recruiters': _data = data.payload.recruiters[0];
                break;
        }
        this.data = _data;
    };
    ListBase.prototype.getListData = function() 
    {
        return $.ajax({type: "GET",url:this.dataUrl+this.id}); 
    };
    ListBase.prototype.addListData = function() 
    {
        return $.ajax({type: "POST",url:this.addDataUrl,data: this.data}); 
    };
    ListBase.prototype.updateListData = function() 
    {
        return $.ajax({type: "POST",url:this.updateDataUrl, data: this.data}); 
    };

    ListBase.prototype.switchTab = function(el) 
    {
        if(!this.getId() && el.attr('data-tab') !== 'add') {
            alert("Please select an item first");
            return;
        }
        var inputs = $('#list_form_wrap').find('.list-form-item input, .list-form-item textarea, .list-form-item select');
        switch(el.attr('data-tab')) {
            case 'display':
                $('#list_form_wrap').addClass('hidden').siblings().removeClass('hidden');
                break;
            case 'edit':
                this.editList(inputs, this.data);
                $('#list_wrap').addClass('hidden').siblings().removeClass('hidden');
                break;
            case 'add':
                this.initList(inputs);
                $('#list_wrap').addClass('hidden').siblings().removeClass('hidden');
                break;
            case 'delete':
                alert('delete not implemented yet');
                break;
        }
        el.addClass('selected').siblings().removeClass('selected');
        $('#side-content').removeClass('loading');
            
    };
  
    ListBase.prototype.displayList = function() 
    {
        var that = this;
            var el = $('#list_wrap').find('.list-item');
            for(var x = 0;x < el.length;x++) {
                var name = $(el[x]).attr('data-name');
                if(typeof name !== 'undefined' || name !== null) {
                    var str = null;
                    switch(name) {
                        case 'recruit_name':
                            str = replaceNullValue(that.data.recruit_first_name + ' '+that.data.recruit_last_name);
                            break;
                        case 'date':
                            str = formatDate(that.data[ name ]);
                            break;
                        case 'salary':
                            str = that.data.salary_from+" - "+that.data.salary_to+" "+that.data.salary_type;
                            break;
                        case 'job_descript':
                            str = replaceNullValue(that.data[ name ]);
                            str = str.replace( /\n/g, "<br/>" );
                            break;
                        default:
                            str = replaceNullValue(that.data[ name ]);
                            break;
                    }
                    $(el[x]).find('.list-content').html(str);
                }
            }
            that.switchTab($('#option_display'));   
    };
    ListBase.prototype.saveList = function() 
    {
        var that = this;
        if(!this.id) {
            this.addListData().done(function(_data) {
                if(_data.payload.success) {
                    that.setId(_data.payload.id);
                    that.getListData().done(function(_data) {
                        that.cacheData(_data);
                        that.addToList();
                        that.displayList();
                    });
                }
            }).fail(function(_response){
                if(!parseInt(_response.responseJSON.logged_in)) { 
                        window.location.reload(); 
                    } else {
                        alert(_response.responseJSON.payload.err_msg);
                    }
            });
        } else {
            this.updateListData().done(function(_data) {
                if(_data.payload.success) {
                    var job_item_elem = $('#list').find('.listed-item[data-id="'+_data.payload.id+'"]');
                    that.displayList(job_item_elem);
                }                 
            }).fail(function(_response){
                if(!parseInt(_response.responseJSON.logged_in)) { 
                        window.location.reload(); 
                    } else {
                        alert(_response.responseJSON.payload.err_msg);
                    }
            });
        }
    };
    ListBase.prototype.editList = function(el, _data) 
    {
        if(_data === null) { return; }
                for(var x = 0;x < el.length;x++) {
                    var name = $(el[x]).attr('name');
                    if(typeof name !== 'undefined') {
                        if(name === 'salary_type') {
                              $(el[x]).find('option[value="'+_data[ name ]+'"').attr('selected','selected');
                        } else {
                            $(el[x]).val(_data[ name ]);
                        }
                    }
                }
       
    };
    ListBase.prototype.initList = function(el) 
    {  
        for(var x = 0;x < el.length;x++) {
            var name = $(el[x]).attr('name');
            if(typeof name !== 'undefined') {
	    	if(name !== 'salary_id') {
                	$(el[x]).val('');
		} else {
			$(el[x]).val('1');
		}
            }
        }
        this.setId(0);        
    };

// End of ListBase class
function ListJobBoard(listType) 
{
    ListBase.call(this, listType);
    this.dataUrl = "/jobs/view/";
    this.addDataUrl = "/jobs/add/";
    this.updateDataUrl = "/jobs/update/";
    this.recruiterUrl =  "/recruiters/view/";
    this.recruiterId = 0;
    this.bindRecruiterAutofill();
};
ListJobBoard.prototype = Object.create(ListBase.prototype);

    ListJobBoard.prototype.addToList = function() 
    {
    	var htmlChunk = [];
    	htmlChunk.push('<div class="listed-item invisible" data-id="'+this.data.id+'">');
    	htmlChunk.push('<div class="job-title">');
    	htmlChunk.push('<h4 class="job-title-item">'+this.data.job_title+'</h4>');
    	htmlChunk.push('<p class="job-title-item pipe-divider"><span class="fa fa-calendar"></span>'+formatDate(this.data.date)+'</p>');
    	htmlChunk.push('</div>');
    	htmlChunk.push('<div class="recruiter">');
    	htmlChunk.push('<h5>Recruiter Details</h5>');
    	htmlChunk.push('<p>Name: <span class="labelstyle">'+replaceNullValue(this.data.recruit_first_name+' '+this.data.recruit_last_name)+'</span></p>');
    	htmlChunk.push('<p>Company: <span class="labelstyle">'+replaceNullValue(this.data.recruit_company)+'</span></p>');
    	htmlChunk.push('</div>');
    	htmlChunk.push('<div class="job_details">');
    	htmlChunk.push('<h5>Company Details</h5>');
    	htmlChunk.push('<p>Co. Name: <span class="labelstyle">'+replaceNullValue(this.data.company_name)+'</span></p>');
    	htmlChunk.push('<p>Co. Person: <span class="labelstyle">'+replaceNullValue(this.data.company_person)+'</span></p>');
    	htmlChunk.push('</div>');
    	htmlChunk.push('</div><div class="list-divider"></div>');
        var that = this;
        if($('#emptylist').length) {
            $('#list').html(htmlChunk.join(''));
        } else {
            $('#list').find('.listed-item').filter(':first').before(htmlChunk.join(''));
        }
        var job_item_elem = $('#list').find('.listed-item[data-id="'+this.data.id+'"]');
        job_item_elem.on('click',function() {
            that.listAction($(this));
        });
        
        that.displayList();
        setTimeout(function() {
            job_item_elem.removeClass('invisible');
        },400);
    }
    
    ListJobBoard.prototype.getRecruiterData = function() 
    { 
        return $.ajax({type: "GET",url:this.recruiterUrl+this.recruiterId}); 
    }
    ListJobBoard.prototype.populateRecruiterFields = function(scope) 
    {
        var r_id = parseInt(scope.find('option:selected').val());
        var that = this;
        if(this.recruiterId !== r_id) {
            this.recruiterId = r_id;
            scope.next('.min-loading').removeClass('hidden');
            this.getRecruiterData().done(function(data) {
                if(data.payload.success) {
                    var inputs = $('#list_form_wrap').find('.list-form-item.recruiter-section input');
                    that.editList(inputs, data.payload.recruiters[0]);
                    scope.next('.min-loading').addClass('hidden');
                }                
            }).fail(function(_response){
                if(!parseInt(_response.responseJSON.logged_in)) { 
                        window.location.reload(); 
                    } else {
                        alert(_response.responseJSON.payload.err_msg);
                        scope.next('.min-loading').addClass('hidden');
                    }
            });
         }
    };
    ListJobBoard.prototype.bindRecruiterAutofill = function()
    {
        var that = this;
        $('#recruiter_select').on('change',function() {
            that.populateRecruiterFields($(this));
        });
    }
function ListRecruiterBoard(listType) 
{
    ListBase.call(this, listType);
    this.dataUrl = "/recruiters/view/";
    this.addDataUrl = "/recruiters/add/";
    this.updateDataUrl = "/recruiters/update/";
};
ListRecruiterBoard.prototype = Object.create(ListBase.prototype);

    ListRecruiterBoard.prototype.addToList = function() 
    {
    	var htmlChunk = [];
    	htmlChunk.push('<div class="listed-item invisible" data-id="'+this.data.id+'">');
    	htmlChunk.push('<div class="recruiter">');
    	htmlChunk.push('<span class="fa fa-user"></span><span class="pipe-divider"></span>');
        if(((this.data.recruit_first_name)+(this.data.recruit_last_name)).length) {
            htmlChunk.push('<span class="labelstyle">'+this.data.recruit_first_name+' '+this.data.recruit_last_name+'</span>');
            if((this.data.recruit_company).length) {
            htmlChunk.push('<span class="labelstyle"> - </span>');    	  
        }
        }
        if((this.data.recruit_company).length) {
            htmlChunk.push('<span class="labelstyle">'+this.data.recruit_company+'</span>');    	  
        }
        htmlChunk.push('</div>');
    	htmlChunk.push('</div>');
    	htmlChunk.push('</div><div class="list-divider"></div>');
        var that = this;
        if($('#emptylist').length) {
            $('#list').html(htmlChunk.join(''));
        } else {
            $('#list').find('.listed-item').filter(':first').before(htmlChunk.join(''));
        }
        var job_item_elem = $('#list').find('.listed-item[data-id="'+this.data.id+'"]');
        job_item_elem.on('click',function() {
            that.listAction($(this));
        });
        
        that.displayList();
        setTimeout(function() {
            job_item_elem.removeClass('invisible');
        },400);
    }
    
    ListRecruiterBoard.prototype.getRecruiterData = function() 
    { 
        return $.ajax({type: "GET",url:this.recruiterUrl+this.recruiterId}); 
    }
    
   
function jobDashboard(){
    var joblist = new ListJobBoard('jobs');
}
function recruiterDashboard(){
    var recruiterlist = new ListRecruiterBoard('recruiters');
}

function appify() {
    var page = document.getElementById('content');
    var pageChild = $(page).find('.container-body');
    var jqobj = pageChild.add(page);
    var pageElemHeight,viewHeight;
    
    pageElemHeight = page.offsetTop;
    viewHeight = $(window).height();
    $(jqobj).height(viewHeight - pageElemHeight);

    $(window).on('resize',function () {
        if(this.innerHeight !== viewHeight) {
                viewHeight = this.innerHeight;
                $(jqobj).height(viewHeight - pageElemHeight);   
        }
    });

}
function Popup(){
}

    Popup.prototype.init = function() {
    
    }
function Search(searchType) 
{
    this.id = 0;
    this.dataUrl;
    this.searchValue;
    this.searchType = searchType;
    this.data = null;
    this.init();
}
    Search.prototype.init = function()
    {
        this.scope = $('input.search');
        this.initInput();
        this.bindCloseButton();
        var that = this;
        this.scope.on('keyup', function() {
            var val = $(this).val();
            if(val.length > 2) {
                if(!that.scope.siblings('.search-close').hasClass('hidden')) { 
                    that.scope.siblings('.search-close').addClass('hidden');
                }
                that.scope.siblings('.min-loading').removeClass('hidden');
                that.searchValue = val;
                that.search();
            }
            
        });
    }
    Search.prototype.search = function()
    {
        var that = this;
        this.getSearchData().done(function(_data){
            if(_data.payload.success){
                that.cacheData(_data);
                that.displayResults();
                that.scope.siblings('.min-loading').addClass('hidden')
                          .siblings('.search-close').removeClass('hidden');
                
            } 
        }).fail(function(_response){
            if(!parseInt(_response.responseJSON.logged_in)) { 
                window.location.reload(); 
            } else { 
                that.noResults(_response);
                that.scope.siblings('.min-loading').addClass('hidden')
                              .siblings('.search-close').removeClass('hidden');
            }
        });
    }
    Search.prototype.initInput = function() 
    {
        this.scope.val('');
    }
    Search.prototype.bindCloseButton = function() 
    {
        var that = this;
        this.scope.siblings('.search-close').on('click', function(){
            that.closeResults();
        });
    }
    Search.prototype.noResults = function(data) 
    {
        var msg = data.responseJSON.payload.err_msg;
        var searchEl =  this.scope.siblings('.search-results');
        var searchElInner = searchEl.find('.search-results-inner');
        if(searchEl.hasClass('hidden')) { searchEl.removeClass('hidden');}
        searchElInner.html('<div class="search-err-msg"><span class="fa fa-times"></span><span class="pipe-divider"></span>'+msg+'</div>');
        
    }
    Search.prototype.cacheData = function(data) 
    {
        var _data;
        switch(this.searchType){
            case 'jobs': _data = data.payload.jobs[0];
                break;
            case 'recruiters': _data = data.payload.recruiters;
                break;
        }
        this.data = _data;
    }
    Search.prototype.getSearchData = function() 
    {
        return $.ajax({type: "POST",url:this.dataUrl,data:{'search_value':this.searchValue}}); 
    }
    Search.prototype.htmlFactory = function(data)
    {
        var cssClass;
        switch(this.searchType){
            case 'jobs': cssClass = 'fa-list-alt';
                break;
            case 'recruiters': cssClass = 'fa-user';
                break;
        }
        var htmlChunk = [];
        htmlChunk.push('<div class="search-item" data-id="'+data.id+'">');
        if($.trim(data.recruit_name) !== '') {
            htmlChunk.push('<div class="search-holder"><span class="fa '+cssClass+'"></span><span class="pipe-divider"></span>'+$.trim(data.recruit_name)+'</div>');
        } else {
            htmlChunk.push('<div class="search-holder"><span class="fa '+cssClass+'"></span><span class="pipe-divider"></span>'+data.recruit_company+'</div>');
        }
        htmlChunk.push('</div>');
        
        return htmlChunk.join('');
    }
    Search.prototype.bindResults = function(el)
    {
        var that = this;
        var elements = el.find('.search-item');
        elements.on('click', function() {
            var id = parseInt($(this).attr('data-id'));
            if(that.id !== id) {;
                that.id = id;
                that.getRecruiterData(id).done(function(_data){
                    if(_data.payload.success){
                        elements.off('click');
                        el.html('');
                        that.closeResults();
                        that.populateFields(_data.payload.recruiters[0]);
                    }
                }).fail(function(_response){
                    if(!parseInt(_response.responseJSON.logged_in)) { 
                        window.location.reload(); 
                    } else {
                        alert(_response.responseJSON.payload.err_msg);
                    }
                });
            } 
        });
    }
    Search.prototype.openResults = function() 
    {   
        if(!this.data) {return;}
        var searchEl = this.scope.siblings('.search-results');
        searchEl.removeClass('hidden');
       
    }
    Search.prototype.closeResults = function() 
    {   
        var searchEl = this.scope.siblings('.search-results');
        searchEl.addClass('hidden').find('.search-results-inner').html('');
        this.scope.siblings('.search-close').addClass('hidden');
        this.initInput();
    }
    Search.prototype.displayResults = function() 
    {
        if(this.data === null) { return; }
        var list =[];
        for(var x = 0; x < this.data.length; x++){
            list.push(this.htmlFactory(this.data[x]));
        }
        var searchEl =  this.scope.siblings('.search-results');
        var searchElInner = searchEl.find('.search-results-inner');
        searchElInner.html(list.join(''));
        this.openResults();
        this.bindResults(searchElInner);
    }
    Search.prototype.populateFields = function(_data) 
    {
        if(_data === null) { return; }
         var inputs = $('#list_form_wrap').find('.list-form-item.recruiter-section input');
                for(var x = 0;x < inputs.length;x++) {
                    var name = $(inputs[x]).attr('name');
                    if(typeof name !== 'undefined') {
                        $(inputs[x]).val(_data[ name ]);
                    }
                }
            //scope.next('.min-loading').removeClass('hidden');
            // scope.next('.min-loading').addClass('hidden');
         
    };

var SearchRecruiter = function(searchType) 
{
    Search.call(this,searchType);
    this.dataUrl = '/recruiters/search/';
    this.recruiterUrl =  "/recruiters/view/";
}
SearchRecruiter.prototype = Object.create(Search.prototype);

    SearchRecruiter.prototype.getRecruiterData = function(recruiter_id) 
    { 
        return $.ajax({type: "GET",url:this.recruiterUrl+recruiter_id}); 
    }

function Dropdown(){
}

    Dropdown.prototype.init = function() {
    
    }
    
function pageController() {
    var el = $('#page').attr('data-page');
var paginate, post, display, displayIt;
    switch(el) {
        case'jobs':
            jobDashboard();
            appify();
            var search = new SearchRecruiter('recruiters');
            break;
        case 'recruiters':
            recruiterDashboard();
            appify();
            break;
    }   
}

$(document).ready( function() { pageController(); });