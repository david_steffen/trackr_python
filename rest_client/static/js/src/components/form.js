var iForm = Ractive.extend({
    template: '#iForm',
    initItem: function() {
    	var item = initItemData(this);
    	this.set('item', item);
    },
    data: { 
        item:null,
        fixed: false,
    },
    clearForm: function() {
        this.initItem();
        clearForm(this);
        this.set({open:false,edit:false});
    },
    oninit: function() {
    	
        this.initItem();
        this.on({
        	toggleForm: function(event){
	            this.toggle('open');
	            if(this.get('edit')) {
	                this.set('edit',false)
	            }
	        },
            cancelForm: function() {
                this.clearForm();
            },
            submitItem: function(event) {
                var item = this.get('item'), self = this;
                if(!task) { return };
                var updateFlag = this.get('edit');
                var method = (updateFlag) ? 'PUT' : 'POST';
                
                var csrftoken = $.cookie('csrftoken');
                $.ajax({type: method,url: url,data: item,
                	  beforeSend: function( xhr ) {
                		    xhr.setRequestHeader( 'X-CSRFToken', csrftoken);
                		  }
                })
                .done(function(resp){
                    if(resp){
                        var item = resp;
                        if(updateFlag) {
                            self.updateItems(item);
                        } else {
                            self.addItems(item);
                        }
                    } else {
                    	console.log(resp);
                    }
                }).fail(function(resp){
                        console.log(resp);
                });
                return false;
            },
            handleInput: function(event) {
                var input = this.get('item');
                input[ event.original.target.name ] = event.original.target.value;
                this.set(input);
            },
            handleCheckbox: function(event) {
                var input = this.get('item');
                input[ event.original.target.name ] = $(event.original.target).prop('checked');
                this.set(input);
            },
            
            deleteItem: function(event) {
                if( confirm("Are you sure you wish to delete this item?")) {
                    var item = this.get('item'), self = this;
                    if(!item) { return };
                    var method = 'DELETE';
                    var csrftoken = $.cookie('csrftoken');
                    $.ajax({type: method,url: url,data: {id: item.id},beforeSend: function( xhr ) {
            		    xhr.setRequestHeader( 'X-CSRFToken', csrftoken);
          		  }})
                    .done(function(resp){
                        if(resp.payload.success){
                            self.deleteItems(resp);
                        } else {
                            console.log(resp);
                        }
                    }).fail(function(resp){
                            failure(resp.responseJSON.payload);
                    });
                }
            },
        });
    },
    addItems: function(item) {
        var items = this.get('items');
        this.unshift('items',item);
        this.clearForm();
    },
    updateItems: function(item) {
        var items = this.get('items');
        for(var x in items) {
            if(items[x].id === item.id) {
                this.splice( 'items', x, 1, item );
                break;
            }
        }
        this.clearForm();
    },
    deleteItems: function(task) {
        var items = this.get('items');
        for(var x in items) {
            if(items[x].id === item.id) {
                this.splice( 'items', x, 1);
                break;
            }
        }
        this.clearForm();
    },
    onrender: function() {
    	var formEl = this.nodes.fixedForm;
	    var top = formEl.getBoundingClientRect().y;
	    var self = this;
	    $(window).scroll(function (event) {
	        var y = event.currentTarget.pageYOffset;
	        toggleFixedForm(y,top);
	    });
	    var toggleFixedForm = function(y,topOffset) {
	        if (y >= topOffset) {
	            // if so, ad the fixed class
	            self.set('fixed', true);
	        } else {
	            // otherwise remove it
	            self.set('fixed', false);
	        }
	    };
    },
    
});