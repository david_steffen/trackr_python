var Dropdown = Ractive.extend({
    template: '#dropdown',
    data: {
        active: false,
        selected_id:  0,
        default_id: 0,
        dd_items: [],
    },

    oninit: function() {
    	this.loadDropdownItems();
        this.on({
            toggleSelect: function(event) {
                this.toggle('active');
            },
            selectItem: function(event) {
                this.set('active',false);
                this.set('selected_id',event.context.id);
            }
        });
    },
    loadDropdownItems: function() {
    	var self = this;
		loadDropdownData()
			.done(function(resp){
		        if(resp.length){
		        	var default_id = resp[0].id;
		        	var items = resp.reduce(function(acc, item) {
	                    acc[item.id] = {id: item.id, item: item};
	                    return acc;
	                }, {});
		        	self.set({selected_id: default_id, default_id: default_id,dd_items:items,});
		        } else {
		//            self.setState({error: true});
		        }
		    }).fail(function(resp){
		//        self.setState({error: true});
		//        self.setState({err_msg: _data.responseJSON.payload.err_msg});
		    });

    },

});