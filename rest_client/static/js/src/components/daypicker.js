	var DayPicker = Ractive.extend({
    template: '#dayPicker',
    data: {
        dates: null,
        lastDate: null,
        currentDate: null,
        numberOfDays: 5,
        reset: false,
        selectedDate: null,
        formatJSDate: function(date) {
                var date = new Date(date);
                var formattedDate =  DateHelper.recentDays(date);
                return formattedDate;
        },
    },
    onchange: function(prop) {
        if(typeof prop.selectedDate !== 'undefined' && !DateHelper.isJSTimestamp(prop.selectedDate)) {
            var t_stamp = DateHelper.createJSTimestamp(prop.selectedDate);
            var dateObj = new Date(t_stamp);
            this.set('selectedDate', DateHelper.getTimestampString(dateObj))
            this.setDates(dateObj,this.get('numberOfDays'));
        }
        if(typeof prop.reset !== 'undefined' && prop.reset) {
            var dateObj = new Date();
            this.set('selectedDate', DateHelper.getTimestampString(dateObj));
            this.setDates(dateObj,this.get('numberOfDays'));
            this.set('reset',false);
        }
    },
    oninit: function() {
        this.on({
            selectDate: function(event){
                this.set('selectedDate',event.context.timestamp);
                var unixTime = DateHelper.createUnixTimestamp(event.context.timestamp);
                this.parent.set('task.task_date',unixTime);
            },
            previousDates: function(event) {
                var dateObj = new Date(this.get('lastDate'));
                this.setDates(dateObj,this.get('numberOfDays'));
            },
            nextDates: function(event) {
                var dateObj = new Date(this.get('currentDate'));
                this.set({lastDate: DateHelper.getTimestampString(dateObj)});
                var datesArray =  DateHelper.getNextDates(dateObj, this.get('numberOfDays'));
                this.set({dates: datesArray});
                var currentDate = DateHelper.getTimestampString(dateObj);
                this.set({currentDate: currentDate});
            }
        })
        var screenWidth = $( window ).width();
        var numberOfDays;
        if(screenWidth <= 768) {
            this.set({numberOfDays: 3});
        } else {
            this.set({numberOfDays: 5});
        }
        var dateObj = new Date();
        this.set('selectedDate', dateObj.getTime());
        this.setDates(dateObj,this.get('numberOfDays'));
    },
    setDates: function(dateObj, numberOfDays) {
        this.set({currentDate: DateHelper.getTimestampString(dateObj)});
        var datesArray =  DateHelper.getPreviousDates(dateObj, numberOfDays);
        this.set({dates: datesArray});
        var lastDate = DateHelper.getTimestampString(dateObj);
        this.set({lastDate: lastDate});
    },
    
});