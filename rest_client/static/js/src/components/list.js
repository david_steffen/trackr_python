var iList = Ractive.extend({
    template: '#iList',
    data: { 
        items: [],
        formatDate: function(date) {
            var t_stamp = DateHelper.createJSTimestamp(date);
            var date = new Date(t_stamp);
            var formattedDate =  DateHelper.recentDays(date);
            return formattedDate;
        },
        
    },
    oninit: function() {
    	this.loadListItems();
	    this.on({
	        editTask: function(event) {
	            var item = event.keypath;
	            var parent = this.parent;
	            parent.findComponent('iForm').set('item',this.get(item));
	            parent.set({edit: true,open: true});
//	            parent.findComponent('ProjectSelect').set('project_id',this.get(task).project);
//	            parent.findComponent('DayPicker').set('selectedDate',this.get(task).task_date);
//	            console.log(parent.findComponent('DayPicker'))
	        },
	        
	    })
    },
    loadListItems: function() {
    	var self = this;
    	loadListData().done(function(resp){
            if(resp.length){
            	var items = sortListData(resp, self);
            	
            	self.set('items',items);
            }
        }).fail(function(resp){
            console.log(resp);
        });
    },
    
});