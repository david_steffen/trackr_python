

var url = '/api/timelogs/';

function initItemData(ractive) {
	var task = {};
    task.task_date = DateHelper.createUnixTimestamp(new Date().getTime());
    task.time = '';
    task.description = '';
    task.logged = 'false';
//    task.project = ractive.root.findComponent('Dropdown').get('default_id');
    return task;
}
function clearForm(ractive) {
	ractive.findComponent('DayPicker').set('reset', true);
    var dropdown = ractive.findComponent('Dropdown');
    dropdown.set('selected_id', dropdown.get('default_id'));
    ractive.findComponent('iFrom').set('task.project',  dropdown.get('default_id'))
}

function loadDropdownData() {
    return $.ajax({type: "GET",url:'api/projects/'});
}

function loadListData()  {
    return $.ajax({type: "GET",url:url});
}

function sortListData(data, ractive) {
	
	for(x in data) {
		data[x].project = ractive.root.findComponent('Dropdown').get('dd_items.'+data[x].project).item;
	}

	return data.reverse();
}
