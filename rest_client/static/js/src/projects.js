var DonutChart = Ractive.extend({
    template: '#DonutChart',
    oninit: function () {
        var self = this, delay = this.get( 'delay' );

        // wait a bit, then animate in
        setTimeout( function () {
            self.animate( 'c', Math.PI * 2, {
                duration: 800,
                easing: 'easeOut'
            });
        }, delay );
    },

    data: {
        c: 0, // we animate from zero to Math.PI * 2 (the number of radians in a circle)
        innerRadius: 20,
        outerRadius: 45,
        selected: null,
        plot: function ( segment ) {
            var innerRadius = this.get( 'innerRadius' );
            var outerRadius = this.get( 'outerRadius' );
            var c = this.get( 'c' ); // allows us to animate intro

            var start = segment.start * c;
            var end = segment.end * c;

            function getPoint ( angle, radius ) {
                return ( ( radius * Math.sin( angle ) ).toFixed( 2 ) + ',' + ( radius * -Math.cos( angle ) ).toFixed( 2 ) );
            }

            var points = [];
            var angle;

            // get points along the outer edge of the segment
            for ( angle = start; angle < end; angle += 0.05 ) {
                points.push( getPoint( angle, outerRadius ) );
            }

            points.push( getPoint( end, outerRadius ) );

            // get points along the inner edge of the segment
            for ( angle = end; angle > start; angle -= 0.05 ) {
                points.push( getPoint( angle, innerRadius ) );
            }

            points.push( getPoint( start, innerRadius ) );

            // join them up as an SVG points list
            return points.join( ' ' );
        }
    },

    computed: {
        segments: function () {
            var points = this.get( 'points' );

            var keys = Object.keys( points ).sort( function ( a, b ) {
                return points[b] - points[a];
            });

            // tally up the total value
            var total = keys.reduce( function ( total, id ) {
                return total + points[id];
            }, 0 );

            // find the start and end point of each segment
            var start = 0;

            return keys.map( function ( id ) {
                var size = points[id] / total, end = start + size, segment;

                segment = {
                    id: id,
                    start: start,
                    end: end
                };

                start = end;
                return segment;
            });
        }
    },
});


var ractive = new Ractive({
    el: '#projects',
    template: '#template',
    data: { 
        projects: [],
        open: false,
        edit: false,
        fixed: false,
        details: null,
        
        calcTimes: function(details, project_id, date) {
            return this.addTimes(details, project_id, date);
        },
        id: null,
        colors: null,
        delay: 50,
        points: null,
    },
    addTimes: function(details, project_id, date) {
        if(!details) { return 0; }
            var total_time = 0;
            for(var x in details) {
                if(project_id === details[x].id) {
                    var project_details = details[x].project_details;
                    for(var y in project_details) {
                        var detail = project_details[y];
                        if(detail.date >= date){
                            total_time += parseFloat(detail.time);
                        }
                    }
                    break;
                }
            }
            return total_time;
    },
    components: { 
        DonutChart: DonutChart,
    },
    oninit: function() {
        var self = this;
        $.ajax({type: "GET",url:'/projects/details/'}).done(function(resp){
            if(resp.payload.success){ 
                self.set('projects',resp.payload.projects);
                self.set('details',resp.payload.details);
                self.getActivityPeriod(resp.payload.details,'week');
            }
        }).fail(function(resp){
            console.log(resp.responseJSON.payload);
        });

    },
    getActivityPeriod: function(details,period) {
        var dateObj = new Date();
        if(period === 'week'){
            var date = DateHelper.createUnixTimestamp(dateObj.setDate(dateObj.getDate()-7));
        }
        var points ={};
        for(var x in details) {
            var id = details[x].id;
            var project;
            var projects = this.get('projects');
            var total_time = 0;
            var project_details = details[x].project_details;
            
            for(var y in project_details) {
                var detail = project_details[y];
                if(detail.date >= date){
                    total_time += parseFloat(detail.time);
                }
            }
            if(total_time) {
                for(var y in projects) {
                    if(id === projects[y].id) {
                        this.set('colors.'+projects[y].abbreviation, projects[y].colour);
                        project = projects[y].abbreviation;
                        break;
                    }
                }
                points[ project ] = total_time;
            }
//            break;
            
        }
        this.set('points', points);
    },
    populateChartData: function(details, project_id, date) {
        var points ={};
        for(var x in details) {
            var id = details[x].id;
            var project;
            var projects = this.get('projects');
            for(var y in projects) {
                if(id === projects[y].id) {
                    this.set('colors.'+projects[y].abbreviation, projects[y].colour);
                    project = projects[y].abbreviation;
                    break;
                }
            }
            var dateObj = new Date();
            var weekAgo = DateHelper.createUnixTimestamp(dateObj.setDate(dateObj.getDate()-7));
            points[ project ] = this.addTimes(details, id, weekAgo) ;
        }
        this.set('points', points);
        console.log(this.get('points'))
    },
});

