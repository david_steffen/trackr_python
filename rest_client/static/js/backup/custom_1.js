


function PaginateJobs() {

}

PaginateJobs.prototype.post = function() {
  
$('#paginate').on("click",".page", function(event) {
    event.preventDefault();
    var link = $(this).attr('href');
    var equalPosition = link.indexOf('='); //Get the position of '='
    var number = link.substring(equalPosition + 1);
var url = getUrl()+"postscripts/jobList_paginate.php";
    $.post(url, {page_no : number}, function(data) {
        var pgtxt = $(data).find( '#pgtxt' );
        var pagination = $(data).find( '.page' );
        $('#paginate').empty(this).append(pgtxt).append(pagination);
            var jobs = $(data).find('.post');
     $('#joblist').empty(this).append(jobs);
    }).error(function(data) {alert(data);});
});
}




function jobAppDashboard() {
    this.job_id = 0;
    this.recruiter_id = 0;
    this.dataUrl = "/jobs/view/";
    this.addDataUrl = "/jobs/add/";
    this.updateDataUrl = "/jobs/update/";
    this.recruiterUrl =  "/recruiters/view/"
    this.data = null;
    this.recruiterData = null;
}
jobAppDashboard.prototype = {
    constructor : jobAppDashboard,
    init: function() {
        var that = this;
        $('#joblist .joblist-item').on("click", function() {
            that.listAction($(this));
        });
        $('#job_form_wrap').find('.save-job').on('click',function() {
            that.saveJob();
        });
        $('#detail_options').find('.job-form-tab').on('click',function() {
            that.switchTab($(this));
        });
        $('#recruiter_select').on('change',function() {
            that.populateRecruiterFields($(this));
        });
        that.switchTab($('#option_add'));
        
    },
    listAction: function(el) {
        var that = this;
        $('#option_display').addClass('current').siblings().removeClass('current');
        var job_id = parseInt(el.attr('data-id'));
        el.addClass('selected').siblings().removeClass('selected');
        if(job_id !==  that.job_id) {
            $('#side-content').addClass('loading').find('.job-entry-item').addClass('hidden');
            that.setJobID(job_id);
            that.getJobData().done(function(dt) {
                if(!parseInt(dt.logged_in)) { 
                    window.location.reload(); 
                } else if(dt.payload.success){
                    that.data = dt.payload.jobs[0];
                    that.setRecruiterID(that.data.recruiter_id);
                    that.displayJob();
                } else {
                    alert(dt.payload.err_msg);
                }
               });
        }
    },
    setJobID : function(job_id) {
        this.job_id = job_id;
    },
    getJobID : function() {
        return this.job_id;
    },
    setRecruiterID : function(recruiter_id) {
        this.recruiter_id = recruiter_id;
    },
    getRecruiterID : function() {
        return this.recruiter_id;
    },
    replaceNullValue : function(str) {
    	var trimstr = $.trim(str);
            if(trimstr === null || trimstr === '' || trimstr === 'null null') {
                return 'n/a';
            } else {
                return trimstr;
            } 
    },
    formatDate: function (str) {
    	var t = str.split(/[- :]/);
    	var date = new Date(t[0], t[1] - 1, t[2], t[3], t[4], t[5]);
    	var formatDate = date.getDate()+'/'+(parseInt(date.getMonth())+1)+'/'+date.getFullYear();
    	return formatDate;
    },
    addToJoblist: function() {
    	var htmlChunk = [];
    	htmlChunk.push('<div class="joblist-item invisible" data-id="'+this.data.id+'">');
    	htmlChunk.push('<div class="job-title">');
    	htmlChunk.push('<h4 class="job-title-item">'+this.data.job_title+'</h4>');
    	htmlChunk.push('<p class="job-title-item"><i class="fa fa-calendar"></i>'+this.formatDate(this.data.date)+'</p>');
    	htmlChunk.push('</div>');
    	htmlChunk.push('<div class="recruiter">');
    	htmlChunk.push('<h5>Recruiter Details</h5>');
    	htmlChunk.push('<p>Name: <span class="labelstyle">'+this.replaceNullValue(this.data.recruit_first_name+' '+this.data.recruit_last_name)+'</span></p>');
    	htmlChunk.push('<p>Company: <span class="labelstyle">'+this.replaceNullValue(this.data.recruit_company)+'</span></p>');
    	htmlChunk.push('</div>');
    	htmlChunk.push('<div class="job_details">');
    	htmlChunk.push('<h5>Company Details</h5>');
    	htmlChunk.push('<p>Co. Name: <span class="labelstyle">'+this.replaceNullValue(this.data.company_name)+'</span></p>');
    	htmlChunk.push('<p>Co. Person: <span class="labelstyle">'+this.replaceNullValue(this.data.company_person)+'</span></p>');
    	htmlChunk.push('</div>');
    	htmlChunk.push('</div><div class="joblist-divider"></div>');
	var that = this;
        if($('#emptylist').length) {
            $('#joblist').html(htmlChunk.join(''));
        } else {
            $('#joblist').find('.joblist-item').filter(':first').before(htmlChunk.join(''));
        }
        var job_item_elem = $('#joblist').find('.joblist-item[data-id="'+this.data.id+'"]');
        job_item_elem.on('click',function() {
            that.listAction($(this));
        });
        
        that.displayJob();
        setTimeout(function() {
            job_item_elem.removeClass('invisible');
        },400);
    },
    saveJob: function() {
        var scope = $('#job_form_wrap');
        if(!this.job_id) {
            this.data = {};
        }
        if(this.getRecruiterID()){
            this.data['recruiter_id'] = this.getRecruiterID();
        } else {
            this.data['recruiter_id'] = 0;
        }
        var el = scope.find('.job-form-item input, .job-form-item textarea, .job-form-item select');
        for(var x = 0;x <= el.length;x++) {
            var name = $(el[x]).attr('name');
            if(typeof name !== 'undefined') {
                var el_val = $.trim($(el[x]).val());
                if(name === 'job_title' && el_val === '') {
                    alert('Please enter the job title');
                    return;
                }
                if (this.data[ name ] !== el_val) {
                    this.data[ name ]= el_val;
                }
            }
        }
        var that = this;
	if(!this.job_id) {
            this.addJobData().done(function(data) {
                if(data.payload.success) {
                    that.setJobID(data.payload.job_id);
                    that.setRecruiterID(data.payload.recruiter_id);
                    that.getJobData().done(function(job_data) {
                        that.data = job_data.payload.jobs[0];
                        that.addToJoblist();
                    });
                } else {
                    alert(data.payload.err_msg);
                }
            });
	} else {
            this.updateJobData().done(function(data) {
                if(data.payload.success) {
                    that.setJobID(data.payload.job_id);
                    that.setRecruiterID(data.payload.recruiter_id);
                    var job_item_elem = $('#joblist').find('.joblist-item[data-id="'+data.payload.job_id+'"]');
                    that.displayJob(job_item_elem);
                } else {
                    alert(data.payload.err_msg);
                }
                
            });
	}
    },
    editJob: function(el) {
        if(this.data === null) { return; }
                for(var x = 0;x <= el.length;x++) {
                    var name = $(el[x]).attr('name');
                    if(typeof name !== 'undefined') {
                        if(name === 'salary_type') {
                              $(el[x]).find('option[value="'+this.data[ name ]+'"').attr('selected','selected')
                        } else {
                            $(el[x]).val(this.data[ name ]);
                        }
                    }
                }
       
    },
    initJob : function(el) {  
        for(var x = 0;x <= el.length;x++) {
            var name = $(el[x]).attr('name');
            if(typeof name !== 'undefined') {
	    	if(name !== 'salary_id') {
                	$(el[x]).val('');
		} else {
			$(el[x]).val('1');
		}
            }
        }
        this.job_id = 0        
    },
    getJobData: function() { return $.ajax({type: "GET",url:this.dataUrl+this.job_id}); },
    addJobData : function() { return $.ajax({type: "POST",url:this.addDataUrl,data: this.data}); },
    updateJobData : function() { return $.ajax({type: "POST",url:this.updateDataUrl,data: this.data}); },
    getRecruiterData : function() { return $.ajax({type: "GET",url:this.recruiterUrl+this.recruiter_id}); },
    switchTab: function(el) {
        if(!this.job_id && el.attr('data-tab') !== 'add') {
            alert("Please select a job first");
            return;
        }
        var inputs = $('#job_form_wrap').find('.job-form-item input, .job-form-item textarea, .job-form-item select');
        switch(el.attr('data-tab')) {
            case 'display':
                $('#job_form_wrap').addClass('hidden').siblings().removeClass('hidden');
                break;
            case 'edit':
                this.editJob(inputs);
                $('#job_list_wrap').addClass('hidden').siblings().removeClass('hidden');
                break;
            case 'add':
                this.initJob(inputs);
                $('#job_list_wrap').addClass('hidden').siblings().removeClass('hidden');
                break;
            case 'delete':
                alert('delete not implemented yet');
                break;
        }
        el.addClass('selected').siblings().removeClass('selected');
        $('#side-content').removeClass('loading');
            
    },
    displayJob : function() {
        var that = this;
            var el = $('#job_list_wrap').find('.job-list-item');
            for(var x = 0;x <= el.length;x++) {
                var name = $(el[x]).attr('data-name');
                if(typeof name !== 'undefined' || name !== null) {
                    var str = null;
                    switch(name) {
                        case 'recruit_name':
                            str = that.replaceNullValue(that.data.recruit_first_name + ' '+that.data.recruit_last_name);
                            break;
                        case 'date':
                            str = that.formatDate(that.data[ name ]);
                            break;
                        case 'salary':
                            str = that.data.salary_from+" - "+that.data.salary_to+" "+that.data.salary_type;
                            break;
                        case 'job_descript':
                            str = that.replaceNullValue(that.data[ name ]);
                            str = str.replace( /\n/g, "<br/>" );
                            break;
                        case 'recruiter_id':
                            that.setRecruiterID(that.data[ name ]);
                            console.log(that.getRecruiterID());
                            break;
                        default:
                            str = that.replaceNullValue(that.data[ name ]);
                            break;
                    }
                    $(el[x]).find('.job-list-content').html(str);
                }
            }
            that.switchTab($('#option_display'));   
    },
    updateRecruiterData : function(el) {
        if(this.recruiterData === null) { return; }
        for(var x = 0;x <= el.length;x++) {
                    var name = $(el[x]).attr('name');
                    if(typeof name !== 'undefined') {
                            $(el[x]).val(this.recruiterData[ name ]);
                        
                    }
                }
    },
    populateRecruiterFields: function(scope) {
        var r_id = parseInt(scope.find('option:selected').val());
        var that = this;
        if(this.recruiter_id !== r_id) {
            this.setRecruiterID(r_id);
            scope.next('.min-loading').removeClass('hidden');
            this.getRecruiterData().done(function(data) {
                if(data.payload.success) {
                    var inputs = $('#job_form_wrap').find('.job-form-item.recruiter-section input');
                    that.recruiterData = data.payload.recruiters[0];
                    that.updateRecruiterData(inputs);
                    scope.next('.min-loading').addClass('hidden');
                } else {
                    alert(data.payload.err_msg);
                    scope.next('.min-loading').addClass('hidden');
                }
                
            });
         }
    }
    
};
function appify() {
    var page = document.getElementById('content');
    var pageChild = $(page).find('.container-body');
    var jqobj = pageChild.add(page);
    var pageElemHeight,viewHeight;
    
    pageElemHeight = page.offsetTop;
    viewHeight = $(window).height();
    $(jqobj).height(viewHeight - pageElemHeight);

    $(window).on('resize',function () {
        if(this.innerHeight !== viewHeight) {
                viewHeight = this.innerHeight;
                $(jqobj).height(viewHeight - pageElemHeight);   
        }
    });

}
function popup(){
}

popup.prototype = {
    constructor : popup
    
}
function dashboardInit() {
    var jobApp = new jobAppDashboard();
    jobApp.init();  
}
function pageController() {
    var el = $('#page').attr('data-page');
var paginate, post, display, displayIt;
    switch(el) {
        case'home':
            dashboardInit();
            appify();
            break;
        case 'recruiters':
            appify();
            break;
    }   
}

$(document).ready( function() { pageController(); });